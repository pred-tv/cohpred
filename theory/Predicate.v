Require Import Coq.Structures.Equalities.

Require Import cohpred_theory.Maps.
Require Import cohpred_theory.Common.
Require Import cohpred_theory.HashTree.

Local Open Scope error_monad_scope.
Local Open Scope positive.

Definition asgn := nat -> bool.
(** An assignment is a function from variables to their truth values. *)

(** The [predicate] type defines the path predicates that are used in the [Ieta] and [Igamma]
    instructions. *)
Inductive pred {A}: Type :=
| Ptrue : pred
| Pfalse : pred
| Pundef : pred
| Pbase : A -> pred
| Pand : pred -> pred -> pred
| Por : pred -> pred -> pred
| Pimp : pred -> pred -> pred
| Pnot : pred -> pred.

Module PredicateNotation.

  Declare Scope predicate.

  Notation "A ∧ B" := (Pand A B) (at level 20, left associativity) : predicate.
  Notation "A ∨ B" := (Por A B) (at level 25, left associativity) : predicate.
  Notation "A → B" := (Pimp A B) (at level 30, right associativity) : predicate.
  Notation "¬ A" := (Pnot A) (at level 15) : predicate.
  Notation "⟂" := (Pfalse) : predicate.
  Notation "'T'" := (Ptrue) : predicate.
  Notation "○" := (Pundef) : predicate.

End PredicateNotation.

Import PredicateNotation.
Local Open Scope predicate.
Local Open Scope bool.
Local Open Scope positive.

Definition equiv {A} (a b: @pred A) :=
  Pand (Pimp a b) (Pimp b a).

Definition hash_pred := @pred positive.

Lemma hash_pred_dec :
  forall (a b: hash_pred),
    {a = b} + {a <> b}.
Proof. generalize Pos.eq_dec. generalize bool_dec. repeat decide equality. Defined.

Fixpoint eval_hash_pred (p: hash_pred) (a: PTree.t bool) : option bool :=
  match p with
  | T => Some true
  | ⟂ => Some false
  | ○ => None
  | ¬ p =>
      match eval_hash_pred p a with
      | Some b => Some (negb b)
      | None => None
      end
  | Pbase k => a ! k
  | p1 ∧ p2 =>
    match eval_hash_pred p1 a, eval_hash_pred p2 a with
    | Some p1', Some p2' => Some (p1' && p2')
    | Some false, None => Some false
    | None, Some false => Some false
    | _, _ => None
    end
  | p1 ∨ p2 =>
    match eval_hash_pred p1 a, eval_hash_pred p2 a with
    | Some p1', Some p2' => Some (p1' || p2')
    | Some true, None => Some true
    | None, Some true => Some true
    | _, _ => None
    end
  | p1 → p2 =>
      match eval_hash_pred p1 a, eval_hash_pred p2 a with
      | Some p1', Some p2' => Some (negb p1' || p2')
      | None, Some false => None
      | Some true, None => None
      | _, _ => Some true
      end
  end.

(** The [predicate] type defines the path predicates that are used in the [Ieta] and [Igamma]
    instructions. *)
Inductive pred_bin {A}: Type :=
| PBtrue : pred_bin
| PBfalse : pred_bin
| PBbase : (bool * A) -> pred_bin
| PBand : pred_bin -> pred_bin -> pred_bin
| PBor : pred_bin -> pred_bin -> pred_bin.

Definition hash_pred_bin : Type := @pred_bin positive * @pred_bin positive.

Fixpoint eval_hash_pred_bin1 (p1: @pred_bin positive) (m: PMap.t bool) : bool :=
  match p1 with
  | PBtrue => true
  | PBfalse => false
  | PBbase (b, p) => if b then m!!p else negb m!!p
  | PBand p1 p2 => eval_hash_pred_bin1 p1 m && eval_hash_pred_bin1 p2 m
  | PBor p1 p2 => eval_hash_pred_bin1 p1 m || eval_hash_pred_bin1 p2 m
  end.

Definition eval_hash_pred_bin (p1: hash_pred_bin) (m: PMap.t bool) : (bool * bool) :=
  (eval_hash_pred_bin1 (fst p1) m, eval_hash_pred_bin1 (snd p1) m).

Fixpoint negate (h: @pred_bin positive) : @pred_bin positive :=
  match h with
  | PBtrue => PBfalse
  | PBfalse => PBtrue
  | PBbase (b, p) => PBbase (negb b, p)
  | PBand p1 p2 => PBor (negate p1) (negate p2)
  | PBor p1 p2 => PBand (negate p1) (negate p2)
  end.

Fixpoint conv_hash_pred (p: hash_pred) (h: PTree.t positive) (fresh: positive):
  (hash_pred_bin * PTree.t positive * positive) :=
  match p with
  | Ptrue => ((PBfalse, PBtrue), h, fresh)
  | Pfalse => ((PBfalse, PBfalse), h, fresh)
  | Pundef => ((PBtrue, PBfalse), h, fresh)
  | Pnot p => let '((p1, p2), h', fresh') := conv_hash_pred p h fresh in
              ((p1, negate p2), h', fresh')
  | Pand p1 p2 =>
      let '((p1s, p1'), h', fresh') := conv_hash_pred p1 h fresh in
      let '((p2s, p2'), h'', fresh'') := conv_hash_pred p2 h' fresh' in
      ((PBor (PBand p1' p2s) (PBor (PBand p1s p2s) (PBand p1s p2')),
         PBand p1' p2'), h'', fresh'')
  | Por p1 p2 =>
      let '((p1s, p1'), h', fresh') := conv_hash_pred p1 h fresh in
      let '((p2s, p2'), h'', fresh'') := conv_hash_pred p2 h' fresh' in
      ((PBor (PBand (negate p1') p2s) (PBor (PBand (negate p2') p1s) (PBand p1s p2s)),
         PBor p1' p2'), h'', fresh'')
  | Pimp p1 p2 =>
      let '((p1s, p1'), h', fresh') := conv_hash_pred p1 h fresh in
      let '((p2s, p2'), h'', fresh'') := conv_hash_pred p2 h' fresh' in
      ((PBor (PBand (negate p1s) (PBand p1' p2s)) (PBand (negate p2s) (PBand (negate p2') p1s)),
        PBor p1s (PBor (negate p1') p2')), h'', fresh'')
  | Pbase k =>
      ((PBbase (true, fresh), PBbase (true, k)), PTree.set k fresh h, fresh+1)
  end.

Fixpoint mult {A: Type} (a b: list (list A)) : list (list A) :=
  match a with
  | nil => nil
  | l :: ls => mult ls b ++ (List.map (fun x => l ++ x) b)
  end.

Definition simplify' {A} (p: @pred A) :=
  match p with
  | A ∧ T => A
  | T ∧ A => A
  | _ ∧ ⟂ => ⟂
  | ⟂ ∧ _ => ⟂
  | ○ ∧ ○ => ○
  | A ∨ ⟂ => A
  | ⟂ ∨ A => A
  | _ ∨ T => T
  | T ∨ _ => T
  | ○ ∨ ○ => ○
  | T → A => A
  | ⟂ → _ => T
  | _ → T => T
  | ○ → ○ => T
  | ¬ T => ⟂
  | ¬ ⟂ => T
  | ¬ ○ => ○
  | A => A
  end.

Fixpoint simplify {A} (dec: forall a b: @pred A, {a = b} + {a <> b} ) (p: @pred A) :=
  match p with
  | A ∧ B =>
    let A' := simplify dec A in
    let B' := simplify dec B in
    if dec A' B' then A' else simplify' (A' ∧ B')
  | A ∨ B =>
    let A' := simplify dec A in
    let B' := simplify dec B in
    if dec A' B' then A' else simplify' (A' ∨ B')
  | A → B =>
    let A' := simplify dec A in
    let B' := simplify dec B in
    if dec A' B' then A' else simplify' (A' → B')
  | ¬ p => simplify' (¬ (simplify dec p))
  | T => T
  | ⟂ => ⟂
  | ○ => ○
  | Pbase a => Pbase a
  end.

Fixpoint sat_hash_pred_bin1' (p: @pred_bin positive) (a: asgn) : bool :=
  match p with
  | PBtrue => true
  | PBfalse => false
  | PBbase (b, p') => if b then a (Pos.to_nat p') else negb (a (Pos.to_nat p'))
  | PBand p1 p2 => sat_hash_pred_bin1' p1 a && sat_hash_pred_bin1' p2 a
  | PBor p1 p2 => sat_hash_pred_bin1' p1 a || sat_hash_pred_bin1' p2 a
  end.

Fixpoint sat_hash_pred_bin1 (p: @pred_bin positive) (a: PMap.t bool) : bool :=
  match p with
  | PBtrue => true
  | PBfalse => false
  | PBbase (b, p') => if b then a !! p' else negb (a !! p')
  | PBand p1 p2 => sat_hash_pred_bin1 p1 a && sat_hash_pred_bin1 p2 a
  | PBor p1 p2 => sat_hash_pred_bin1 p1 a || sat_hash_pred_bin1 p2 a
  end.

Definition sat_hash_pred_bin (p: hash_pred_bin) (a: PMap.t bool) : bool :=
  negb (sat_hash_pred_bin1 (fst p) a) && sat_hash_pred_bin1 (snd p) a.

Definition conv_tree (p: PTree.t positive) (assigns: PTree.t bool) : PMap.t bool :=
  PTree.fold (fun m i a =>
                match assigns ! i with
                | Some true => PMap.set a false (PMap.set i true m)
                | Some false => PMap.set a false (PMap.set i false m)
                | None => PMap.set a true (PMap.set i false m)
                end) p (PMap.init false).

Definition interp_bin (b: bool * bool): option bool :=
  match b with | (true, _) => None | (_, b) => Some b end.

Fixpoint max_hash_pred (p: hash_pred) :=
  match p with
  | T | ⟂ | ○ => 1
  | Pbase k => k
  | p1 ∧ p2 | p1 ∨ p2 | p1 → p2 => Pos.max (max_hash_pred p1) (max_hash_pred p2)
  | ¬ p' => max_hash_pred p'
  end.

Definition eval_through_bin (p: hash_pred) (a: PTree.t bool) :=
  let '(p', h, f) := conv_hash_pred p (PTree.empty _) (max_hash_pred p + 1) in
  let m := conv_tree h a in
  interp_bin (sat_hash_pred_bin1 (fst p') m, sat_hash_pred_bin1 (snd p') m).

Definition combine_option {A} (a b: option A) : option A :=
  match a, b with
  | Some a', _ => Some a'
  | _, Some b' => Some b'
  | _, _ => None
  end.

Definition max_key {A} (t: PTree.t A) :=
  fold_right Pos.max 1 (map fst (PTree.elements t)).

Inductive pred_In {A} : A -> @pred A -> Prop :=
| pred_In_Pbase : forall a, pred_In a (Pbase a)
| pred_In_Pand1 : forall c p1 p2, pred_In c p1 -> pred_In c (p1 ∧ p2)
| pred_In_Pand2 : forall c p1 p2, pred_In c p2 -> pred_In c (p1 ∧ p2)
| pred_In_Por1 : forall c p1 p2, pred_In c p1 -> pred_In c (p1 ∨ p2)
| pred_In_Por2 : forall c p1 p2, pred_In c p2 -> pred_In c (p1 ∨ p2)
| pred_In_Pimp1 : forall c p1 p2, pred_In c p1 -> pred_In c (p1 → p2)
| pred_In_Pimp2 : forall c p1 p2, pred_In c p2 -> pred_In c (p1 → p2)
| pred_In_Pnot : forall c p, pred_In c p -> pred_In c (¬ p).

Definition pred_In_dec :
  forall A (c: A) p,
    (forall a b : A, { a = b } + { a <> b }) ->
    { pred_In c p } + { ~ pred_In c p }.
Proof.
  induction p; crush.
  - right. unfold not. intros. inv H.
  - right. unfold not. intros. inv H.
  - right. unfold not. intros. inv H.
  - destruct (X c a); subst.
    { left. constructor. }
    { right. unfold not. intros. inv H. auto. }
  - pose proof X as X1. pose proof X as X2.
    apply IHp1 in X1. apply IHp2 in X2.
    inv X1; inv X2. left. constructor. tauto.
    left. constructor. auto.
    left. apply pred_In_Pand2. auto.
    right. unfold not. intros. inv H1; auto.
  - pose proof X as X1. pose proof X as X2.
    apply IHp1 in X1. apply IHp2 in X2.
    inv X1; inv X2. left. constructor. tauto.
    left. constructor. auto.
    left. apply pred_In_Por2. auto.
    right. unfold not. intros. inv H1; auto.
  - pose proof X as X1. pose proof X as X2.
    apply IHp1 in X1. apply IHp2 in X2.
    inv X1; inv X2. left. constructor. tauto.
    left. constructor. auto.
    left. apply pred_In_Pimp2. auto.
    right. unfold not. intros. inv H1; auto.
  - pose proof X as X1. apply IHp in X1.
    inv X1. left. constructor. tauto.
    right. unfold not. intros. inv H0; auto.
Defined.

Lemma pred_not_in_Pand :
  forall A (c: A) p1 p2,
    ~ pred_In c (p1 ∧ p2) ->
    ~ pred_In c p1 /\ ~ pred_In c p2.
Proof.
  intros. split.
  - unfold not in *. intros. apply H. constructor. auto.
  - unfold not in *. intros. apply H. apply pred_In_Pand2. auto.
Qed.

Lemma pred_not_in_Por :
  forall A (c: A) p1 p2,
    ~ pred_In c (p1 ∨ p2) ->
    ~ pred_In c p1 /\ ~ pred_In c p2.
Proof.
  intros. split.
  - unfold not in *. intros. apply H. constructor. auto.
  - unfold not in *. intros. apply H. apply pred_In_Por2. auto.
Qed.

Lemma simplify'_correct :
  forall h a,
    eval_hash_pred (simplify' h) a = eval_hash_pred h a.
Proof. destruct h; crush; repeat (destruct_match; crush). Qed.

Lemma eval_hash_pred_Pand_destr :
  forall p1 p2 p1' p2' a,
    eval_hash_pred p1 a = eval_hash_pred p1' a ->
    eval_hash_pred p2 a = eval_hash_pred p2' a ->
    eval_hash_pred (Pand p1 p2) a = eval_hash_pred (Pand p1' p2') a.
Proof. intros. simpl. rewrite H in *. rewrite H0 in *. auto. Qed.

Lemma eval_hash_pred_Pand_symm :
  forall p1 p2 a,
    eval_hash_pred (Pand p1 p2) a = eval_hash_pred (Pand p2 p1) a.
Proof. simplify. repeat destruct_match; auto. Qed.

Lemma eval_hash_pred_Por_symm :
  forall p1 p2 a,
    eval_hash_pred (Por p1 p2) a = eval_hash_pred (Por p2 p1) a.
Proof. simplify. repeat destruct_match; auto. Qed.

Lemma eval_hash_pred_Por_destr :
  forall p1 p2 p1' p2' a,
    eval_hash_pred p1 a = eval_hash_pred p1' a ->
    eval_hash_pred p2 a = eval_hash_pred p2' a ->
    eval_hash_pred (Por p1 p2) a = eval_hash_pred (Por p1' p2') a.
Proof. intros. simpl. rewrite H in *. rewrite H0 in *. auto. Qed.

Lemma eval_hash_pred_Pand_refl :
  forall p1 a,
    eval_hash_pred (Pand p1 p1) a = eval_hash_pred p1 a.
Proof. intros. simpl. repeat destruct_match; auto. Qed.

Lemma eval_hash_pred_Por_refl :
  forall p1 a,
    eval_hash_pred (Por p1 p1) a = eval_hash_pred p1 a.
Proof. intros. simpl. repeat destruct_match; auto. Qed.

  Lemma pand_false :
  forall a b l,
    eval_hash_pred (Pand a b) l <> Some true ->
    eval_hash_pred b l = Some true ->
    eval_hash_pred a l <> Some true.
Proof. simplify. repeat destruct_match; crush. Qed.

Lemma pand_true :
  forall a b l,
    eval_hash_pred (Pand a b) l = Some true ->
    eval_hash_pred a l = Some true /\ eval_hash_pred b l = Some true.
Proof. simplify; repeat destruct_match; crush. Qed.

Lemma pand_true2 :
  forall a b l,
    eval_hash_pred b l = Some true ->
    eval_hash_pred a l = Some true ->
    eval_hash_pred (Pand a b) l = Some true.
Proof. simplify; repeat destruct_match; crush. Qed.

Lemma eval_hash_pred_fold_Pand :
  forall l x la,
    eval_hash_pred (fold_left Pand l x) la = Some true ->
    eval_hash_pred x la = Some true.
Proof.
  induction l as [| el l' IHl ]; [tauto|].
  intros * EVAL. cbn in *.
  eapply IHl in EVAL. eapply pand_true in EVAL; tauto.
Qed.

Lemma eval_hash_pred_fold_Pand2 :
  forall l x la y,
    eval_hash_pred (fold_left Pand l x) la = Some true ->
    eval_hash_pred y la = Some true ->
    eval_hash_pred (fold_left Pand l y) la = Some true.
Proof.
  induction l as [| el l' IHl ]; [tauto|].
  intros * EVAL EVALY. cbn in *.
  pose proof EVAL as EVAL2.
  apply eval_hash_pred_fold_Pand in EVAL.
  eapply IHl; eauto. apply pand_true2; apply pand_true in EVAL; tauto.
Qed.

Lemma eval_hash_pred_fold_Pand3 :
  forall l x la,
    eval_hash_pred (fold_left Pand l x) la = Some true ->
    eval_hash_pred (fold_left Pand l T) la = Some true.
Proof. eauto using eval_hash_pred_fold_Pand2. Qed.

Lemma eval_hash_pred_fold_Pand4 :
  forall l x la,
    eval_hash_pred (fold_left Pand l x) la = Some true ->
    eval_hash_pred (fold_left Pand l T) la = Some true /\ eval_hash_pred x la = Some true.
Proof.
  intros; split;
    eauto using eval_hash_pred_fold_Pand3, eval_hash_pred_fold_Pand.
Qed.

Lemma eval_equiv2 :
  forall a b la,
    eval_hash_pred a la = eval_hash_pred b la ->
    eval_hash_pred (equiv a b) la = Some true.
Proof.
  unfold equiv; intros. unfold eval_hash_pred in *. fold eval_hash_pred in *.
  repeat destruct_match; try discriminate; crush.
Qed.

Lemma eval_equiv :
  forall a b la,
    eval_hash_pred (equiv a b) la = Some true ->
    eval_hash_pred a la = eval_hash_pred b la.
Proof.
  unfold equiv; intros. unfold eval_hash_pred in *. fold eval_hash_pred in *.
  repeat destruct_match; try discriminate; crush.
Qed.

Lemma eval_hash_pred_T_Pand :
  forall a l,
    eval_hash_pred (Pand T a) l = eval_hash_pred a l.
Proof.
  intros; unfold eval_hash_pred; fold eval_hash_pred;
  destruct_match; auto.
Qed.

Lemma fold_and_forall :
  forall p a pt,
    eval_hash_pred (fold_left Pand p pt) a = Some true ->
    Forall (fun x => eval_hash_pred x a = Some true) p.
Proof.
  induction p; crush.
  constructor.
  - apply eval_hash_pred_fold_Pand in H. eapply pand_true; eauto.
  - eapply IHp. eauto.
Qed.

Lemma fold_and_forall2 :
  forall p a pt,
    eval_hash_pred pt a = Some true ->
    Forall (fun x => eval_hash_pred x a = Some true) p ->
    eval_hash_pred (fold_left Pand p pt) a = Some true.
Proof.
  induction p; crush. inv H0.
  eapply IHp; eauto.
  apply pand_true2; eauto.
Qed.

Lemma fold_and_forall3 :
  forall p a,
    Forall (fun x => eval_hash_pred x a = Some true) p <->
      eval_hash_pred (fold_left Pand p T) a = Some true.
Proof.
  intros; split; intros.
  eapply fold_and_forall2; eauto.
  eapply fold_and_forall; eauto.
Qed.

Lemma eval_hash_pred_gso2 :
  forall p x y b,
    ~ pred_In y p ->
    eval_hash_pred p (PTree.set y b x) = eval_hash_pred p x.
Proof.
  induction p; try solve [crush].
  - intros * MAX. cbn in *. rewrite PTree.gso; auto. unfold not; intros.
    apply MAX; subst; constructor.
  - intros. cbn in H. simplify. erewrite !IHp1.
    erewrite !IHp2. auto. unfold not in *; intros; apply H; now apply pred_In_Pand2.
    unfold not in *; intros; apply H; now constructor.
  - intros. cbn in H. simplify. erewrite !IHp1.
    erewrite !IHp2. auto. unfold not in *; intros; apply H; now apply pred_In_Por2.
    unfold not in *; intros; apply H; now constructor.
  - intros. cbn in H. simplify. erewrite !IHp1.
    erewrite !IHp2. auto. unfold not in *; intros; apply H; now apply pred_In_Pimp2.
    unfold not in *; intros; apply H; now constructor.
  - intros. cbn in H. simplify. erewrite !IHp. auto.
    unfold not in *; intros; apply H; now constructor.
Qed.

Lemma eval_hash_pred_gso :
  forall p x y b,
    (max_hash_pred p < y)%positive ->
    eval_hash_pred p (PTree.set y b x) = eval_hash_pred p x.
Proof.
  induction p; try solve [crush].
  - intros * MAX. cbn in *. now rewrite PTree.gso by lia.
  - intros. cbn in H. simplify. erewrite !IHp1 by lia.
    now erewrite !IHp2 by lia.
  - intros. cbn in H. simplify. erewrite !IHp1 by lia.
    now erewrite !IHp2 by lia.
  - intros. cbn in H. simplify. erewrite !IHp1 by lia.
    now erewrite !IHp2 by lia.
  - intros. cbn in H. simplify. now erewrite !IHp by lia.
Qed.

Lemma eval_hash_pred_get :
  forall p a,
    eval_hash_pred (Pbase p) a = a ! p.
Proof. crush. Qed.

Lemma eval_hash_pred_except :
  forall p a a0 a0',
    ~ pred_In a p ->
    (forall x : positive, x <> a -> a0 ! x = a0' ! x) ->
    eval_hash_pred p a0 = eval_hash_pred p a0'.
Proof.
  induction p; crush.
  - destruct (peq a0 a); subst.
    + exfalso. apply H. constructor.
    + now rewrite H0 by auto.
  - erewrite IHp1; eauto.
    erewrite IHp2; eauto.
    unfold not; intros; apply H. apply pred_In_Pand2; auto.
    unfold not; intros; apply H. constructor; auto.
  - erewrite IHp1; eauto.
    erewrite IHp2; eauto.
    unfold not; intros; apply H. apply pred_In_Por2; auto.
    unfold not; intros; apply H. constructor; auto.
  - erewrite IHp1; eauto.
    erewrite IHp2; eauto.
    unfold not; intros; apply H. apply pred_In_Pimp2; auto.
    unfold not; intros; apply H. constructor; auto.
  - erewrite IHp; eauto.
    unfold not; intros; apply H. constructor; auto.
Qed.

Lemma eval_hash_pred_pand :
  forall p p1 p2 p3 a a',
    eval_hash_pred p1 a = eval_hash_pred p a' ->
    eval_hash_pred p2 a = eval_hash_pred p3 a' ->
    eval_hash_pred (p1 ∧ p2) a = eval_hash_pred (p ∧ p3) a'.
Proof. simplify; rewrite !H; now rewrite !H0. Qed.

Lemma eval_hash_pred_por :
  forall p p1 p2 p3 a a',
    eval_hash_pred p1 a = eval_hash_pred p a' ->
    eval_hash_pred p2 a = eval_hash_pred p3 a' ->
    eval_hash_pred (p1 ∨ p2) a = eval_hash_pred (p ∨ p3) a'.
Proof. simplify; rewrite !H; now rewrite !H0. Qed.

Lemma eval_hash_pred_pimp :
  forall p p1 p2 p3 a a',
    eval_hash_pred p1 a = eval_hash_pred p a' ->
    eval_hash_pred p2 a = eval_hash_pred p3 a' ->
    eval_hash_pred (p1 → p2) a = eval_hash_pred (p → p3) a'.
Proof. simplify; rewrite !H; now rewrite !H0. Qed.

Lemma eval_hash_pred_pnot :
  forall p p' a a',
    eval_hash_pred p a = eval_hash_pred p' a' ->
    eval_hash_pred (¬ p) a = eval_hash_pred (¬ p') a'.
Proof. simplify; now rewrite !H. Qed.

Lemma fold_left_Pand_rev :
  forall l b,
    eval_hash_pred (fold_left Pand (rev l) T) b = Some true ->
    eval_hash_pred (fold_left Pand l T) b = Some true.
Proof.
  intros.
  apply fold_and_forall3.
  apply fold_and_forall3 in H.
  apply Forall_forall; intros.
  now eapply Forall_forall in H; [|rewrite <- in_rev; eassumption].
Qed.

Lemma fold_left_Pand_rev2 :
  forall l b,
    eval_hash_pred (fold_left Pand l T) b = Some true ->
    eval_hash_pred (fold_left Pand (rev l) T) b = Some true.
Proof.
  intros.
  apply fold_and_forall3.
  apply fold_and_forall3 in H.
  apply Forall_forall; intros.
  now eapply Forall_forall in H; [|rewrite in_rev; eassumption].
Qed.

Lemma eval_hash_pred_T_Pand2 :
  forall a b l,
    eval_hash_pred b l = Some true ->
    eval_hash_pred (Pand a b) l = eval_hash_pred a l.
Proof.
  intros; unfold eval_hash_pred; fold eval_hash_pred.
    repeat (destruct_match; crush).
Qed.

Lemma eval_hash_pred_T_Pand3:
  forall b a,
    eval_hash_pred (b ∧ T) a = eval_hash_pred b a.
Proof.
  unfold eval_hash_pred; crush.
  repeat (destruct_match; crush).
Qed.

Lemma max_key_correct :
  forall A h_tree hi (c: A),
    h_tree ! hi = Some c ->
    hi <= max_key h_tree.
Proof.
  unfold max_key. intros. apply PTree.elements_correct in H.
  apply max_key_correct'.
  eapply in_map with (f := fst) in H. auto.
Qed.

Module ThreeValued(Base: UsualDecidableTypeBoth).

Definition A := Base.t.
Definition el_dec: forall a b: A, {a = b} + {a <> b} := Base.eq_dec.

Module Import AH := HashTree(Base).
Module Import AH_Prop := HashTreeProperties(Base).

Section EVAL.

Context (eval: A -> option bool).

Definition predicate := @pred A.

(* This evaluation follows Łukasiewicz logic. *)
Fixpoint eval_predicate (p: predicate) : option bool :=
  match p with
  | T => Some true
  | ⟂ => Some false
  | ○ => None
  | ¬ p =>
      match eval_predicate p with
      | None => None
      | Some b => Some (negb b)
      end
  | Pbase c =>
    match eval c with
    | Some p => Some p
    | None => None
    end
  | p1 ∧ p2 =>
    match eval_predicate p1, eval_predicate p2 with
    | Some p1', Some p2' => Some (p1' && p2')
    | Some false, None => Some false
    | None, Some false => Some false
    | _, _ => None
    end
  | p1 ∨ p2 =>
    match eval_predicate p1, eval_predicate p2 with
    | Some p1', Some p2' => Some (p1' || p2')
    | Some true, None => Some true
    | None, Some true => Some true
    | _, _ => None
    end
  | p1 → p2 =>
      match eval_predicate p1, eval_predicate p2 with
      | Some p1', Some p2' => Some (negb p1' || p2')
      | None, Some false => None
      | Some true, None => None
      | _, _ => Some true
      end
  end.

Definition hash_tree := PTree.t A.

Lemma comp_list_dec :
  forall (a b: list positive),
    {a = b} + {a <> b}.
Proof. generalize Pos.eq_dec. decide equality. Defined.

Lemma predicate_dec :
  forall (a b: predicate),
    {a = b} + {a <> b}.
Proof. generalize el_dec. generalize bool_dec. repeat decide equality. Defined.

Fixpoint hash_predicate (p: predicate) (h: PTree.t A)
  : hash_pred * PTree.t A :=
  match p with
  | T => (T, h)
  | ⟂ => (⟂, h)
  | ○ => (○, h)
  | ¬ p => let (p', t) := hash_predicate p h in (¬ p', t)
  | Pbase c =>
      match find_tree c h with
      | Some p => (Pbase p, h)
      | None =>
          let nkey := max_key h + 1 in
          (Pbase nkey, PTree.set nkey c h)
      end
  | p1 ∧ p2 =>
      let (p1', t1) := hash_predicate p1 h in
      let (p2', t2) := hash_predicate p2 t1 in
      (p1' ∧ p2', t2)
  | p1 ∨ p2 =>
      let (p1', t1) := hash_predicate p1 h in
      let (p2', t2) := hash_predicate p2 t1 in
      (p1' ∨ p2', t2)
  | p1 → p2 =>
      let (p1', t1) := hash_predicate p1 h in
      let (p2', t2) := hash_predicate p2 t1 in
      (p1' → p2', t2)
  end.

(*Compute eval_hash_pred (Pand (Por (Pbase 1) (Pbase 2)) (Pbase 3))
        (PTree.set 1 true (PTree.set 3 true (PTree.empty _))).
Compute eval_through_bin (Pand (Por (Pbase 1) (Pbase 2)) (Pbase 3))
        (PTree.set 1 true (PTree.set 3 true (PTree.empty _))).*)

Lemma hash_constant :
  forall p h h_tree hi c h_tree',
    h_tree ! hi = Some c ->
    hash_predicate p h_tree = (h, h_tree') ->
    h_tree' ! hi = Some c.
Proof.
  induction p; crush.
  - repeat (destruct_match; crush); subst.
    pose proof H as X. apply max_key_correct in X.
    rewrite PTree.gso by lia; auto.
  - repeat (destruct_match; crush). exploit IHp1; eauto.
  - repeat (destruct_match; crush). exploit IHp1; eauto.
  - repeat (destruct_match; crush). exploit IHp1; eauto.
  - repeat (destruct_match; crush). exploit IHp; eauto.
Qed.

Lemma hash_predicate_hash_le :
  forall p n p' n',
    hash_predicate p n = (p', n') ->
    (PTree_Properties.cardinal n <= PTree_Properties.cardinal n')%nat.
Proof.
  induction p; crush.
  - repeat (destruct_match; crush).
    assert (n ! (max_key n + 1) = None).
    { destruct (n ! (max_key n + 1)) eqn:?; auto.
      apply max_key_correct in Heqo0. lia. }
    exploit PTree_Properties.cardinal_set; eauto.
    instantiate (1 := a); intros. lia.
  - repeat (destruct_match; crush).
    apply IHp1 in Heqp. apply IHp2 in Heqp0. lia.
  - repeat (destruct_match; crush).
    apply IHp1 in Heqp. apply IHp2 in Heqp0. lia.
  - repeat (destruct_match; crush).
    apply IHp1 in Heqp. apply IHp2 in Heqp0. lia.
  - repeat (destruct_match; crush).
    apply IHp in Heqp0. lia.
Qed.

Lemma hash_predicate_hash_len :
  forall p n p' n',
    hash_predicate p n = (p', n') ->
    PTree_Properties.cardinal n = PTree_Properties.cardinal n' ->
    n = n'.
Proof.
  induction p; crush.
  - repeat (destruct_match; crush).
    assert (n ! (max_key n + 1) = None).
    { destruct (n ! (max_key n + 1)) eqn:?; auto.
      apply max_key_correct in Heqo0. lia. }
    exploit PTree_Properties.cardinal_set; eauto.
    instantiate (1 := a); intros. lia.
  - repeat (destruct_match; crush).
    pose proof Heqp as X; apply hash_predicate_hash_le in X.
    pose proof Heqp0 as X0; apply hash_predicate_hash_le in X0.
    exploit IHp1; eauto. lia. intros; subst.
    exploit IHp2; eauto.
  - repeat (destruct_match; crush).
    pose proof Heqp as X; apply hash_predicate_hash_le in X.
    pose proof Heqp0 as X0; apply hash_predicate_hash_le in X0.
    exploit IHp1; eauto. lia. intros; subst.
    exploit IHp2; eauto.
  - repeat (destruct_match; crush).
    pose proof Heqp as X; apply hash_predicate_hash_le in X.
    pose proof Heqp0 as X0; apply hash_predicate_hash_le in X0.
    exploit IHp1; eauto. lia. intros; subst.
    exploit IHp2; eauto.
  - repeat (destruct_match; crush).
    pose proof Heqp0 as X; apply hash_predicate_hash_le in X.
    exploit IHp; eauto.
Qed.

Lemma pred_In_hash :
  forall p n p' n' c,
    hash_predicate p n = (p', n') ->
    pred_In c p ->
    exists t, n' ! t = Some c /\ pred_In t p'.
Proof.
  induction p; simplify.
  - inv H0.
  - inv H0.
  - inv H0.
  - repeat (destruct_match; crush; []); subst.
    destruct_match; simplify.
    { inv H0. apply find_tree_correct in Heqo.
      econstructor; ecrush core. constructor. }
    { inv H0. econstructor. rewrite PTree.gss. crush. constructor. }
  - repeat (destruct_match; crush; []). inv H0.
    exploit IHp1; eauto. simplify. econstructor. simplify. eapply hash_constant; eauto.
    constructor; auto.
    exploit IHp2; eauto. simplify. econstructor; ecrush core. apply pred_In_Pand2. auto.
  - repeat (destruct_match; crush; []). inv H0.
    exploit IHp1; eauto. simplify. econstructor. simplify. eapply hash_constant; eauto.
    constructor; auto.
    exploit IHp2; eauto. simplify. econstructor; ecrush core. apply pred_In_Por2. auto.
  - repeat (destruct_match; crush; []). inv H0.
    exploit IHp1; eauto. simplify. econstructor. simplify. eapply hash_constant; eauto.
    constructor; auto.
    exploit IHp2; eauto. simplify. econstructor; ecrush core. apply pred_In_Pimp2. auto.
  - repeat (destruct_match; crush; []). inv H0. exploit IHp; eauto; simplify.
    econstructor; simplify; eauto. econstructor; auto.
Qed.

Lemma hash_pred_In' :
  forall p h t c n n',
    hash_predicate p n = (h, n') ->
    n ! t = None ->
    n' ! t = Some c ->
    pred_In c p.
Proof.
  induction p; crush.
  - repeat (destruct_match); subst; crush.
    destruct (Pos.eq_dec t (max_key n + 1)); subst.
    { rewrite PTree.gss in H1. simplify. constructor. }
    { rewrite PTree.gso in H1; crush. }
  - repeat (destruct_match); subst; crush.
    destruct (t0 ! t) eqn:?.
    { pose proof Heqo as X. eapply hash_constant in X; eauto. simplify.
      constructor. eapply IHp1; eauto. }
    { apply pred_In_Pand2; eapply IHp2; eauto. }
  - repeat (destruct_match); subst; crush.
    destruct (t0 ! t) eqn:?.
    { pose proof Heqo as X. eapply hash_constant in X; eauto. simplify.
      constructor. eapply IHp1; eauto. }
    { apply pred_In_Por2; eapply IHp2; eauto. }
  - repeat (destruct_match); subst; crush.
    destruct (t0 ! t) eqn:?.
    { pose proof Heqo as X. eapply hash_constant in X; eauto. simplify.
      constructor. eapply IHp1; eauto. }
    { apply pred_In_Pimp2; eapply IHp2; eauto. }
  - repeat (destruct_match); subst; crush.
    exploit IHp; eauto; intros. econstructor; auto.
Qed.

Lemma hash_pred_In :
  forall p h p' h' c n,
    hash_predicate p (PTree.empty _) = (h, n) ->
    hash_predicate p' n = (h', n) ->
    pred_In c p' -> pred_In c p.
Proof.
  intros. exploit pred_In_hash; eauto. simplify.
  eapply hash_pred_In'; eauto.
Qed.

Lemma pred_In_hash2 :
  forall p h n n' t,
    hash_predicate p n = (h, n') ->
    pred_In t h ->
    exists c, n' ! t = Some c /\ pred_In c p.
Proof.
  induction p; simplify.
  - inv H0.
  - inv H0.
  - inv H0.
  - repeat (destruct_match; crush; []); subst.
    destruct_match; simplify.
    { inv H0. apply find_tree_correct in Heqo.
      econstructor; ecrush core. constructor. }
    { inv H0. econstructor. rewrite PTree.gss. crush. constructor. }
  - repeat (destruct_match; crush; []). inv H0.
    exploit IHp1; eauto. simplify. econstructor. simplify. eapply hash_constant; eauto.
    constructor; auto.
    exploit IHp2; eauto. simplify. econstructor; ecrush core. apply pred_In_Pand2. auto.
  - repeat (destruct_match; crush; []). inv H0.
    exploit IHp1; eauto. simplify. econstructor. simplify. eapply hash_constant; eauto.
    constructor; auto.
    exploit IHp2; eauto. simplify. econstructor; ecrush core. apply pred_In_Por2. auto.
  - repeat (destruct_match; crush; []). inv H0.
    exploit IHp1; eauto. simplify. econstructor. simplify. eapply hash_constant; eauto.
    constructor; auto.
    exploit IHp2; eauto. simplify. econstructor; ecrush core. apply pred_In_Pimp2. auto.
  - repeat (destruct_match; crush; []). inv H0.
    exploit IHp; eauto; simplify; repeat econstructor; ecrush core.
Qed.

Lemma hash_constant2 :
  forall p n h n' v c c',
    hash_predicate p n = (h, n') ->
    n ! v = Some c ->
    n' ! v = Some c' ->
    c = c'.
Proof.
  intros.
  eapply hash_constant in H; eauto. rewrite H1 in H. crush.
Qed.

Definition gen_hash_assoc_el s i a :=
  match eval a with
  | Some b => PTree.set i b s
  | None => s
  end.

Definition gen_hash_assoc_map (h: PTree.t A) :=
  PTree.fold gen_hash_assoc_el h (PTree.empty _).

Lemma gen_hash_assoc_map_stable :
  forall l p t,
    ~ In p (map fst l) ->
    (fold_left (fun s ia => gen_hash_assoc_el s (fst ia) (snd ia)) l t) ! p = t ! p.
Proof.
  induction l; crush.
  assert (fst a <> p /\ ~ In p (map fst l)).
  { unfold not; split; intros; eapply H; eauto. }
  simplify.
  replace (t ! p) with ((gen_hash_assoc_el t (fst a) (snd a)) ! p).
  eapply IHl; eauto.
  unfold gen_hash_assoc_el. destruct_match; eauto.
  rewrite PTree.gso; eauto.
Qed.

Lemma gen_hash_assoc_map_corr_l' :
  forall l p t a,
    list_norepet (map fst l) ->
    In (p, a) l ->
    t ! p = None ->
    (fold_left (fun s ia => gen_hash_assoc_el s (fst ia) (snd ia)) l t) ! p = eval a.
Proof.
  induction l; crush; inv H; inv H0.
  - simplify. rewrite gen_hash_assoc_map_stable; eauto.
    unfold gen_hash_assoc_el; destruct_match.
    rewrite PTree.gss; eauto. crush.
  - eapply IHl; eauto. unfold gen_hash_assoc_el.
    assert (p <> (fst a)).
    { unfold not; intros; eapply H4. subst. destruct a. simplify.
      eapply in_map_iff. econstructor. split; eauto. crush. }
    destruct_match; eauto. rewrite PTree.gso; eauto.
Qed.

Lemma gen_hash_assoc_map_corr' :
  forall h p a,
    h ! p = Some a ->
    (gen_hash_assoc_map h) ! p = eval a.
Proof.
  unfold gen_hash_assoc_map; intros; rewrite PTree.fold_spec;
  eauto using gen_hash_assoc_map_corr_l', PTree.elements_keys_norepet,
    PTree.elements_correct, PTree.gempty.
Qed.

Lemma gen_hash_assoc_map_corr''_bis :
  forall p' h' h'',
    (forall i b, h' ! i = Some b -> h'' ! i = Some b) ->
    (forall a, pred_In a p' -> exists b, h' ! a = Some b) ->
    eval_hash_pred p' (gen_hash_assoc_map h'') = eval_hash_pred p' (gen_hash_assoc_map h').
Proof.
  induction p'; crush.
  - assert (pred_In a (Pbase a)) by constructor.
    apply H0 in H1; simplify.
    repeat (erewrite gen_hash_assoc_map_corr'); eauto.
  - erewrite IHp'1; eauto. erewrite IHp'2; eauto.
    intros; eapply H0. eapply pred_In_Pand2; auto.
    intros; eapply H0. constructor; auto.
  - erewrite IHp'1; eauto. erewrite IHp'2; eauto.
    intros; eapply H0. eapply pred_In_Por2; auto.
    intros; eapply H0. constructor; auto.
  - erewrite IHp'1; eauto. erewrite IHp'2; eauto.
    intros; eapply H0. eapply pred_In_Pimp2; auto.
    intros; eapply H0. constructor; auto.
  - erewrite IHp'; eauto; intros. eapply H0; constructor; eauto.
Qed.

Lemma hash_predicate_pred_In :
  forall p h p' h' a,
    hash_predicate p h = (p', h') ->
    pred_In a p' -> exists b, h' ! a = Some b.
Proof.
  induction p; crush.
  - inv H0.
  - inv H0.
  - inv H0.
  - destruct_match; simplify.
    inv H0. econstructor; eauto. eapply find_tree_correct; eauto.
    inv H0. econstructor. rewrite PTree.gss; eauto.
  - destruct (hash_predicate p1 h) eqn:?.
    destruct (hash_predicate p2 t) eqn:?. simplify. inv H0.
    exploit IHp1; eauto; simplify.
    econstructor. eapply hash_constant; eauto.
    exploit IHp2; eauto; simplify.
  - destruct (hash_predicate p1 h) eqn:?.
    destruct (hash_predicate p2 t) eqn:?. simplify. inv H0.
    exploit IHp1; eauto; simplify.
    econstructor. eapply hash_constant; eauto.
    exploit IHp2; eauto; simplify.
  - destruct (hash_predicate p1 h) eqn:?.
    destruct (hash_predicate p2 t) eqn:?. simplify. inv H0.
    exploit IHp1; eauto; simplify.
    econstructor. eapply hash_constant; eauto.
    exploit IHp2; eauto; simplify.
  - destruct (hash_predicate p h) eqn:?; simplify.
    inv H0.
    eapply IHp; eauto.
Qed.

Lemma gen_hash_assoc_map_corr'' :
  forall p h p' h' h'',
    (forall i b, h' ! i = Some b -> h'' ! i = Some b) ->
    hash_predicate p h = (p', h') ->
    eval_hash_pred p' (gen_hash_assoc_map h'') = eval_hash_pred p' (gen_hash_assoc_map h').
Proof. eauto using gen_hash_assoc_map_corr''_bis, hash_predicate_pred_In. Qed.

Lemma wf_hash_table_distr :
  forall p h_tree h h_tree',
    hash_predicate p h_tree = (h, h_tree') ->
    wf_hash_table h_tree ->
    wf_hash_table h_tree'.
Proof.
  induction p; crush; repeat destruct_match; crush; try apply wf_hash_table_set; auto.
  - unfold HashTree.max_key. unfold max_key.
    assert (forall a: positive, a + 1 > a) by lia.
    apply H.
  - eapply IHp2; [eauto | eapply IHp1; eauto].
  - eapply IHp2; [eauto | eapply IHp1; eauto].
  - eapply IHp2; [eauto | eapply IHp1; eauto].
  - eapply IHp; eauto.
Qed.

Lemma gen_hash_assoc_map_corr :
  forall p h p' h',
    wf_hash_table h ->
    hash_predicate p h = (p', h') ->
    eval_hash_pred p' (gen_hash_assoc_map h') = eval_predicate p.
Proof.
  induction p; crush.
  - destruct (find_tree a h) eqn:?; crush.
    apply find_tree_correct in Heqo.
    erewrite gen_hash_assoc_map_corr'; eauto.
    destruct_match; auto.
    erewrite gen_hash_assoc_map_corr'.
    destruct_match; eauto.
    rewrite PTree.gss. auto.
  - destruct (hash_predicate p1 h) eqn:?.
    destruct (hash_predicate p2 t) eqn:?.
    simplify.
    erewrite gen_hash_assoc_map_corr''; [|intros; eapply hash_constant; eauto|]; eauto.
    erewrite IHp1; eauto.
    erewrite IHp2; [auto | | eauto].
    eapply wf_hash_table_distr; eauto.
  - destruct (hash_predicate p1 h) eqn:?.
    destruct (hash_predicate p2 t) eqn:?.
    simplify.
    erewrite gen_hash_assoc_map_corr''; [|intros; eapply hash_constant; eauto|]; eauto.
    erewrite IHp1; eauto.
    erewrite IHp2; [auto | | eauto].
    eapply wf_hash_table_distr; eauto.
  - destruct (hash_predicate p1 h) eqn:?.
    destruct (hash_predicate p2 t) eqn:?.
    simplify.
    erewrite gen_hash_assoc_map_corr''; [|intros; eapply hash_constant; eauto|]; eauto.
    erewrite IHp1; eauto.
    erewrite IHp2; [auto | | eauto].
    eapply wf_hash_table_distr; eauto.
  - destruct (hash_predicate p h) eqn:?; simplify.
    erewrite IHp; eauto.
Qed.

End EVAL.

End ThreeValued.

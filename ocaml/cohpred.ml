open Maps
open Errors
open Camlcoq
open Predicate0

module OSet = Set

(* open Z3 *)
(* open Z3.Arithmetic *)

open Smtcoq_plugin
let error_hole = "The proof contains a hole, which is not supported by the current version of the extracted checker; in a future version, a warning will be output but the remaining of the proof will be checked"
let error_quant = "The current version of the extracted checker does not support quantifiers"
module Mc = CoqInterface.Micromega_plugin_Certificate.Mc

(* let ctx = (mk_context [("model", "true"); ("proof", "false")]) *)

let rec all_vars_rec = function
  | Ptrue   -> []
  | Pfalse  -> []
  | Pundef  -> []
  | Pbase a -> [a]
  | Pand (p1, p2) -> all_vars_rec p1 @ all_vars_rec p2
  | Por (p1, p2)  -> all_vars_rec p1 @ all_vars_rec p2
  | Pimp (p1, p2) -> all_vars_rec p1 @ all_vars_rec p2
  | Pnot p -> all_vars_rec p

(* let rec pred_bin_to_expr = function *)
(*   | PBtrue -> Boolean.mk_true ctx *)
(*   | PBfalse -> Boolean.mk_false ctx *)
(*   | PBbase (b, p) -> *)
(*     if b then *)
(*       Boolean.mk_const_s ctx (Printf.sprintf "p%d" (P.to_int p)) *)
(*     else *)
(*       Boolean.mk_not ctx (Boolean.mk_const_s ctx (Printf.sprintf "p%d" (P.to_int p))) *)
(*   | PBand (a, b) -> *)
(*     Boolean.mk_and ctx [pred_bin_to_expr a; pred_bin_to_expr b] *)
(*   | PBor (a, b) -> *)
(*     Boolean.mk_or ctx [pred_bin_to_expr a; pred_bin_to_expr b] *)

let rec pred_bin_to_expr_s = function
  | PBtrue -> "true"
  | PBfalse -> "false"
  | PBbase (b, p) ->
    if b then Printf.sprintf "p%d" (P.to_int p)
    else Printf.sprintf "(not p%d)" (P.to_int p)
  | PBand (a, b) ->
    Printf.sprintf "(and %s %s)" (pred_bin_to_expr_s a) (pred_bin_to_expr_s b)
  | PBor (a, b) ->
    Printf.sprintf "(or %s %s)" (pred_bin_to_expr_s a) (pred_bin_to_expr_s b)

(* let rec pred_to_expr = function *)
(*   | Ptrue -> Integer.mk_numeral_i ctx 1 *)
(*   | Pfalse -> Integer.mk_numeral_i ctx (-1) *)
(*   | Pundef -> Integer.mk_numeral_i ctx 0 *)
(*   | Pbase p -> Integer.mk_const_s ctx (Printf.sprintf "p%d" (P.to_int p)) *)
(*   | Pand (a, b) -> *)
(*     let a_e = pred_to_expr a in *)
(*     let b_e = pred_to_expr b in *)
(*     Boolean.mk_ite ctx (Arithmetic.mk_lt ctx a_e b_e) a_e b_e *)
(*   | Por (a, b) -> *)
(*     let a_e = pred_to_expr a in *)
(*     let b_e = pred_to_expr b in *)
(*     Boolean.mk_ite ctx (Arithmetic.mk_lt ctx b_e a_e) a_e b_e *)
(*   | Pimp (a, b) -> *)
(*     let a_e = Integer.mk_numeral_i ctx 1 in *)
(*     let b_e = Arithmetic.mk_add ctx [ *)
(*         Arithmetic.mk_sub ctx [Integer.mk_numeral_i ctx 1; pred_to_expr a]; *)
(*         pred_to_expr b *)
(*       ] in *)
(*     Boolean.mk_ite ctx (Arithmetic.mk_lt ctx a_e b_e) a_e b_e *)
(*   | Pnot p -> *)
(*     Arithmetic.mk_sub ctx [Integer.mk_numeral_i ctx 0; pred_to_expr p] *)

let rec pred_to_expr_s fresh = function
  | Ptrue -> (fresh, [], "1")
  | Pfalse -> (fresh, [], "(- 1)")
  | Pundef -> (fresh, [], "0")
  | Pbase p -> (fresh, [], Printf.sprintf "p%d" (P.to_int p))
  | Pand (a, b) ->
    let (fresh', l1, a_e) = pred_to_expr_s fresh a in
    let (fresh'', l2, b_e) = pred_to_expr_s fresh' b in
    (fresh'' + 1,
     Printf.sprintf "(assert (ite (< %s %s) (= p%d %s) (= p%d %s)))"
       a_e b_e fresh'' a_e fresh'' b_e :: l1 @ l2,
     Printf.sprintf "p%d" fresh'')
  | Por (a, b) ->
    let (fresh', l1, a_e) = pred_to_expr_s fresh a in
    let (fresh'', l2, b_e) = pred_to_expr_s fresh' b in
    (fresh'' + 1,
     Printf.sprintf "(assert (ite (>= %s %s) (= p%d %s) (= p%d %s)))"
       a_e b_e fresh'' a_e fresh'' b_e :: l1 @ l2,
     Printf.sprintf "p%d" fresh'')
  | Pimp (a, b) ->
    let (fresh', l1, a_e) = pred_to_expr_s fresh a in
    let (fresh'', l2, b_e) = pred_to_expr_s fresh' b in
    (fresh'' + 1,
     Printf.sprintf "(assert (ite (< 1 (+ (- 1 %s) %s)) (= p%d 1) (= p%d (+ (- 1 %s) %s))))"
       a_e b_e fresh'' fresh'' a_e b_e :: l1 @ l2,
     Printf.sprintf "p%d" fresh'')
  | Pnot p ->
    let (fresh', l, p') = pred_to_expr_s fresh p in
    (fresh', l, Printf.sprintf "(- %s)" p')

(* let pred_bin_unsat p = *)
(*   let solver = (Solver.mk_solver ctx None) in *)
(*   Solver.check solver [pred_bin_to_expr p] = Solver.UNSATISFIABLE *)

(* let pred_unsat p = *)
(*   let solver = (Solver.mk_solver ctx None) in *)
(*   let all_vars = all_vars_rec p in *)
(*   List.iter *)
(*     (fun x -> *)
(*       let n = Printf.sprintf "p%d" (P.to_int x) in *)
(*       Solver.add solver *)
(*         [Boolean.mk_and ctx *)
(*            [Arithmetic.mk_le ctx (Integer.mk_numeral_i ctx (-1)) (Integer.mk_const_s ctx n); *)
(*             Arithmetic.mk_ge ctx (Integer.mk_numeral_i ctx 1) (Integer.mk_const_s ctx n)]]) *)
(*     all_vars ; *)
(*   Solver.add solver *)
(*     [Boolean.mk_not ctx (Boolean.mk_eq ctx (pred_to_expr p) (Integer.mk_numeral_i ctx 1))]; *)
(*   (\* print_endline (Solver.to_string solver); *\) *)
(*   Solver.check solver [] = Solver.UNSATISFIABLE *)

let (--) i j =
    let rec aux n acc =
      if n < i then acc else aux (n-1) (n :: acc)
    in aux j []

module SS = OSet.Make(Int);;

let pred_unsat' p =
  let fresh_in = Camlcoq.P.to_int (Predicate0.max_hash_pred p) + 1 in
  let (fresh_out, asserts, final) = pred_to_expr_s fresh_in p in
  let all_vars =
    (List.map Camlcoq.P.to_int (all_vars_rec p)
     @ fresh_in--(fresh_out-1))
    |> SS.of_list |> SS.to_seq |> List.of_seq
  in
  let oc = open_out "out.smt2" in
  Printf.fprintf oc "(set-logic UFLIA)\n";
  List.iter (fun x ->
      Printf.fprintf oc "(declare-fun p%d () Int)\n(assert (and (<= (- 1) p%d) (<= p%d 1)))\n" x x x
    ) all_vars;
  List.iter (fun x -> Printf.fprintf oc "%s\n" x) asserts;
  Printf.fprintf oc "(assert (not (= %s 1)))\n(check-sat)\n(exit)\n" final;
  close_out oc

(* #################################################################################################### *)

let mkInt = Uint63.of_int

(* From trace/coqTerms.ml *)
let mkArray a =
  let l = (Array.length a) - 1 in
  snd (Array.fold_left (fun (i,acc) c ->
                        let acc' =
                          if i = l then
                            acc
                          else
                            PArray.set acc (mkInt i) c in
                        (i+1,acc')
                       ) (0, PArray.make (mkInt l) a.(l)) a)


(* Generate a list *)
let rec dump_list dump_elt l =
  match l with
    | [] -> []
    | e :: l -> (dump_elt e :: dump_list dump_elt l)

(* Generate a list *)
let rec dump_list' dump_elt l =
  match l with
    | [] -> []
    | e :: l -> (dump_elt e :: dump_list' dump_elt l)


(* Correspondance between the Micromega representation of objects and
   the one that has been extracted *)
let rec dump_nat x =
  match x with
    | Mc.O -> Datatypes.O
    | Mc.S p -> Datatypes.S (dump_nat p)

let rec dump_nat' x =
  match x with
    | Datatypes.O -> Mc.O
    | Datatypes.S p -> Mc.S (dump_nat' p)


let rec dump_positive x =
  match x with
    | Mc.XH -> BinNums.Coq_xH
    | Mc.XO p -> BinNums.Coq_xO (dump_positive p)
    | Mc.XI p -> BinNums.Coq_xI (dump_positive p)

let rec dump_positive' x =
  match x with
    | BinNums.Coq_xH -> Mc.XH
    | BinNums.Coq_xO p -> Mc.XO (dump_positive' p)
    | BinNums.Coq_xI p -> Mc.XI (dump_positive' p)


let dump_z x =
  match x with
    | Mc.Z0 -> BinNums.Z0
    | Mc.Zpos p -> BinNums.Zpos (dump_positive p)
    | Mc.Zneg p -> BinNums.Zneg (dump_positive p)

let dump_z' x =
  match x with
    | BinNums.Z0 -> Mc.Z0
    | BinNums.Zpos p -> Mc.Zpos (dump_positive' p)
    | BinNums.Zneg p -> Mc.Zneg (dump_positive' p)


let dump_pol e =
  let rec dump_pol e =
    match e with
      | Mc.Pc n -> EnvRing.Pc (dump_z n)
      | Mc.Pinj(p,pol) -> EnvRing.Pinj (dump_positive p, dump_pol pol)
      | Mc.PX(pol1,p,pol2) -> EnvRing.PX (dump_pol pol1, dump_positive p, dump_pol pol2) in
  dump_pol e

let dump_pol' e =
  let rec dump_pol' e =
    match e with
      | EnvRing.Pc n -> Mc.Pc (dump_z' n)
      | EnvRing.Pinj(p,pol) -> Mc.Pinj (dump_positive' p, dump_pol' pol)
      | EnvRing.PX(pol1,p,pol2) -> Mc.PX (dump_pol' pol1, dump_positive' p, dump_pol' pol2) in
  dump_pol' e


let dump_psatz e =
  let rec dump_cone e =
    match e with
      | Mc.PsatzIn n -> RingMicromega.PsatzIn (dump_nat n)
      | Mc.PsatzMulC(e,c) -> RingMicromega.PsatzMulC (dump_pol e, dump_cone c)
      | Mc.PsatzSquare e -> RingMicromega.PsatzSquare (dump_pol e)
      | Mc.PsatzAdd(e1,e2) -> RingMicromega.PsatzAdd (dump_cone e1, dump_cone e2)
      | Mc.PsatzMulE(e1,e2) -> RingMicromega.PsatzMulE (dump_cone e1, dump_cone e2)
      | Mc.PsatzC p -> RingMicromega.PsatzC (dump_z p)
      | Mc.PsatzZ -> RingMicromega.PsatzZ in
  dump_cone e

let dump_psatz' e =
  let rec dump_cone' e =
    match e with
      | RingMicromega.PsatzIn n -> Mc.PsatzIn (dump_nat' n)
      | RingMicromega.PsatzMulC(e,c) -> Mc.PsatzMulC (dump_pol' e, dump_cone' c)
      | RingMicromega.PsatzSquare e -> Mc.PsatzSquare (dump_pol' e)
      | RingMicromega.PsatzAdd(e1,e2) -> Mc.PsatzAdd (dump_cone' e1, dump_cone' e2)
      | RingMicromega.PsatzMulE(e1,e2) -> Mc.PsatzMulE (dump_cone' e1, dump_cone' e2)
      | RingMicromega.PsatzC p -> Mc.PsatzC (dump_z' p)
      | RingMicromega.PsatzZ -> Mc.PsatzZ in
  dump_cone' e


let rec dump_proof_term = function
  | CoqInterface.Micromega_plugin_Micromega.DoneProof -> ZMicromega.DoneProof
  | CoqInterface.Micromega_plugin_Micromega.RatProof(cone,rst) ->
    ZMicromega.RatProof (dump_psatz cone, dump_proof_term rst)
  | CoqInterface.Micromega_plugin_Micromega.CutProof(cone,prf) ->
    ZMicromega.CutProof (dump_psatz cone, dump_proof_term prf)
  | CoqInterface.Micromega_plugin_Micromega.EnumProof(c1,c2,prfs) ->
    ZMicromega.EnumProof (dump_psatz c1, dump_psatz c2, dump_list dump_proof_term prfs)
  | CoqInterface.Micromega_plugin_Micromega.ExProof(p,prf) ->
    ZMicromega.ExProof (dump_positive p, dump_proof_term prf)

let rec dump_proof_term' = function
  | ZMicromega.DoneProof -> CoqInterface.Micromega_plugin_Micromega.DoneProof
  | ZMicromega.RatProof(cone,rst) ->
    CoqInterface.Micromega_plugin_Micromega.RatProof (dump_psatz' cone, dump_proof_term' rst)
  | ZMicromega.CutProof(cone,prf) ->
    CoqInterface.Micromega_plugin_Micromega.CutProof (dump_psatz' cone, dump_proof_term' prf)
  | ZMicromega.EnumProof(c1,c2,prfs) ->
    CoqInterface.Micromega_plugin_Micromega.EnumProof (dump_psatz' c1, dump_psatz' c2, dump_list' dump_proof_term' prfs)
  | ZMicromega.ExProof(p,prf) ->
    CoqInterface.Micromega_plugin_Micromega.ExProof (dump_positive' p, dump_proof_term' prf)



(* From trace/coqInterface.ml *)
(* WARNING: side effect on r! *)
let mkTrace step_to_coq next size def_step r =
  let rec mkTrace s =
    if s = size then
      []
    else (
      r := next !r;
      let st = step_to_coq !r in
      (st :: mkTrace (s+1))
    ) in
  (mkTrace 0, def_step)


(* From trace/smtTrace.ml *)
let to_coq to_lit
   (cRes, cWeaken, cImmFlatten,
    cTrue, cFalse, cBuildDef, cBuildDef2, cBuildProj,
    cImmBuildProj,cImmBuildDef,cImmBuildDef2,
    cEqTr, cEqCgr, cEqCgrP,
    cLiaMicromega, cLiaDiseq, cSplArith, cSplDistinctElim,
    cBBVar, cBBConst, cBBOp, cBBNot, cBBEq, cBBDiseq,
    cBBNeg, cBBAdd, cBBMul, cBBUlt, cBBSlt, cBBConc,
    cBBExtr, cBBZextn, cBBSextn,
    cBBShl, cBBShr,
    cRowEq, cRowNeq, cExt,
    cHole, cForallInst) confl =

  let out_f f = to_lit f in
  let out_c c = mkInt (SmtTrace.get_pos c) in
  let out_cl cl = List.fold_right (fun f l -> (out_f f :: l)) cl [] in
  let step_to_coq c =
    match c.SmtCertif.kind with
    | Res res ->
	let size = List.length res.rtail + 3 in
	let args = Array.make size (mkInt 0) in
	args.(0) <- mkInt (SmtTrace.get_pos res.rc1);
	args.(1) <- mkInt (SmtTrace.get_pos res.rc2);
	let l = ref res.rtail in
	for i = 2 to size - 2 do
	  match !l with
	  | c::tl ->
	      args.(i) <- mkInt (SmtTrace.get_pos c);
	      l := tl
	  | _ -> assert false
	done;
        cRes (mkInt (SmtTrace.get_pos c), mkArray args)
    | Other other ->
	begin match other with
        | Weaken (c',l') -> cWeaken (out_c c, out_c c', out_cl l')
	| ImmFlatten (c',f) -> cImmFlatten (out_c c, out_c c', out_f f)
        | True -> cTrue (out_c c)
	| False -> cFalse (out_c c)
	| BuildDef f -> cBuildDef (out_c c, out_f f)
	| BuildDef2 f -> cBuildDef2 (out_c c, out_f f)
	| BuildProj (f, i) -> cBuildProj (out_c c, out_f f, mkInt i)
	| ImmBuildDef c' -> cImmBuildDef (out_c c, out_c c')
	| ImmBuildDef2 c' -> cImmBuildDef2 (out_c c, out_c c')
	| ImmBuildProj(c', i) -> cImmBuildProj (out_c c, out_c c', mkInt i)
        | EqTr (f, fl) ->
          let res = List.fold_right (fun f l -> (out_f f :: l)) fl [] in
          cEqTr (out_c c, out_f f, res)
        | EqCgr (f, fl) ->
          let res = List.fold_right (fun f l -> ((match f with | Some f -> Some (out_f f) | None -> None) :: l)) fl [] in
          cEqCgr (out_c c, out_f f, res)
        | EqCgrP (f1, f2, fl) ->
          let res = List.fold_right (fun f l -> ((match f with | Some f -> Some (out_f f) | None -> None) :: l)) fl [] in
          cEqCgrP (out_c c, out_f f1, out_f f2, res)
	| LiaMicromega (cl,d) ->
          let cl' = List.fold_right (fun f l -> (out_f f :: l)) cl [] in
          let c' = List.fold_right (fun f l -> (dump_proof_term f :: l)) d [] in
          cLiaMicromega (out_c c, cl', c')
        | LiaDiseq l -> cLiaDiseq (out_c c, out_f l)
        | SplArith (orig,res,l) ->
          let res' = out_f res in
          let l' = List.fold_right (fun f l -> (dump_proof_term f :: l)) l [] in
          cSplArith (out_c c, out_c orig, res', l')
	| SplDistinctElim (c',f) -> cSplDistinctElim (out_c c, out_c c', out_f f)
        | BBVar res -> cBBVar (out_c c, out_f res)
        | BBConst res -> cBBConst (out_c c, out_f res)
        | BBOp (c1,c2,res) ->
           cBBOp (out_c c, out_c c1, out_c c2, out_f res)
        | BBNot (c1,res) ->
           cBBNot (out_c c, out_c c1, out_f res)
        | BBNeg (c1,res) ->
           cBBNeg (out_c c, out_c c1, out_f res)
        | BBAdd (c1,c2,res) ->
           cBBAdd (out_c c, out_c c1, out_c c2, out_f res)
        | BBMul (c1,c2,res) ->
           cBBMul (out_c c, out_c c1, out_c c2, out_f res)
        | BBUlt (c1,c2,res) ->
           cBBUlt (out_c c, out_c c1, out_c c2, out_f res)
        | BBSlt (c1,c2,res) ->
           cBBSlt (out_c c, out_c c1, out_c c2, out_f res)
        | BBConc (c1,c2,res) ->
           cBBConc (out_c c, out_c c1, out_c c2, out_f res)
        | BBExtr (c1,res) ->
           cBBExtr (out_c c, out_c c1, out_f res)
        | BBZextn (c1,res) ->
           cBBZextn (out_c c, out_c c1, out_f res)
        | BBSextn (c1,res) ->
           cBBSextn (out_c c, out_c c1, out_f res)
        | BBShl (c1,c2,res) ->
           cBBShl (out_c c, out_c c1, out_c c2, out_f res)
        | BBShr (c1,c2,res) ->
           cBBShr (out_c c, out_c c1, out_c c2, out_f res)
        | BBEq (c1,c2,res) ->
           cBBEq (out_c c, out_c c1, out_c c2, out_f res)
        | BBDiseq (res) -> cBBDiseq (out_c c, out_f res)
        | RowEq (res) -> cRowEq (out_c c, out_f res)
        | RowNeq (cl) ->
          let out_cl cl =
            List.fold_right (fun f l -> (out_f f :: l)) cl []
          in
          cRowNeq (out_c c, out_cl cl)
        | Ext (res) -> cExt (out_c c, out_f res)
        | Hole (prem_id, concl) -> failwith error_hole
        | Forall_inst (cl, concl) | Qf_lemma (cl, concl) -> failwith error_quant
	end
    | _ -> assert false in
  let def_step =
    cRes (mkInt 0, mkArray [|mkInt 0|]) in
  let r = ref confl in
  let nc = ref 0 in
  while not (SmtTrace.isRoot !r.SmtCertif.kind) do r := SmtTrace.prev !r; incr nc done;
  let last_root = !r in
  (* Be careful, step_to_coq makes a side effect on cuts so it needs to be called first *)
  let res = mkTrace step_to_coq SmtTrace.next !nc def_step r in
  (res, last_root)


(* Map OCaml integers to the extracted version of N *)
(* From trace/coqTerms.ml *)
let rec mkNat = function
  | 0 -> Datatypes.O
  | i -> Datatypes.S (mkNat (i-1))

let rec mkPositive = function
  | 1 -> BinNums.Coq_xH
  | i ->
     let c =
       if (i mod 2) = 0 then
         fun c -> BinNums.Coq_xO c
       else
         fun c -> BinNums.Coq_xI c
     in
     c (mkPositive (i / 2))

let mkN = function
  | 0 -> BinNums.N0
  | i -> BinNums.Npos (mkPositive i)


(* Correspondance between the SMTCoq representation of objects and the
   one that has been extracted *)
let rec btype_to_coq = function
  | SmtBtype.TZ ->        SMT_terms.Typ.TZ
  | SmtBtype.Tbool ->     SMT_terms.Typ.Tbool
  | SmtBtype.Tpositive -> SMT_terms.Typ.Tpositive
  | SmtBtype.TBV i -> SMT_terms.Typ.TBV (mkN i)
  | SmtBtype.TFArray (k, v) -> SMT_terms.Typ.TFArray (btype_to_coq k, btype_to_coq v)
  | SmtBtype.Tindex i ->  SMT_terms.Typ.Tindex (mkN (SmtBtype.indexed_type_index i))

(* Correspondance between the SMTCoq representation of objects and the
   one that has been extracted *)
let rec btype_to_coq' = function
  | SMT_terms.Typ.TZ ->        SmtBtype.TZ
  | SMT_terms.Typ.Tbool ->     SmtBtype.Tbool
  | SMT_terms.Typ.Tpositive -> SmtBtype.Tpositive
  | SMT_terms.Typ.TBV i -> SmtBtype.TBV (Camlcoq.N.to_int i)
  | SMT_terms.Typ.TFArray (k, v) -> SmtBtype.TFArray (btype_to_coq' k, btype_to_coq' v)
  | SMT_terms.Typ.Tindex i ->  SmtBtype.Tindex (SmtBtype.dummy_indexed_type (Camlcoq.N.to_int i))


let c_to_coq = function
  | SmtAtom.CO_xH -> SMT_terms.Atom.CO_xH
  | SmtAtom.CO_Z0 -> SMT_terms.Atom.CO_Z0
  | SmtAtom.CO_BV bv ->
     SMT_terms.Atom.CO_BV (dump_list (fun x -> x) bv, mkN (List.length bv))

let c_to_coq' = function
  | SMT_terms.Atom.CO_xH -> SmtAtom.CO_xH
  | SMT_terms.Atom.CO_Z0 -> SmtAtom.CO_Z0
  | SMT_terms.Atom.CO_BV (bv, l) ->
     SmtAtom.CO_BV (dump_list' (fun x -> x) bv)


let u_to_coq = function
  | SmtAtom.UO_xO ->   SMT_terms.Atom.UO_xO
  | SmtAtom.UO_xI ->   SMT_terms.Atom.UO_xI
  | SmtAtom.UO_Zpos -> SMT_terms.Atom.UO_Zpos
  | SmtAtom.UO_Zneg -> SMT_terms.Atom.UO_Zneg
  | SmtAtom.UO_Zopp -> SMT_terms.Atom.UO_Zopp
  | SmtAtom.UO_BVbitOf (s, i) -> SMT_terms.Atom.UO_BVbitOf (mkN s, mkNat i)
  | SmtAtom.UO_BVnot s -> SMT_terms.Atom.UO_BVnot (mkN s)
  | SmtAtom.UO_BVneg s -> SMT_terms.Atom.UO_BVneg (mkN s)
  | SmtAtom.UO_BVextr (s1, s2, s3) -> SMT_terms.Atom.UO_BVextr (mkN s1, mkN s2, mkN s3)
  | SmtAtom.UO_BVzextn (s1, s2) -> SMT_terms.Atom.UO_BVzextn (mkN s1, mkN s2)
  | SmtAtom.UO_BVsextn (s1, s2) -> SMT_terms.Atom.UO_BVsextn (mkN s1, mkN s2)

let u_to_coq' = function
  | SMT_terms.Atom.UO_xO ->   SmtAtom.UO_xO
  | SMT_terms.Atom.UO_xI ->   SmtAtom.UO_xI
  | SMT_terms.Atom.UO_Zpos -> SmtAtom.UO_Zpos
  | SMT_terms.Atom.UO_Zneg -> SmtAtom.UO_Zneg
  | SMT_terms.Atom.UO_Zopp -> SmtAtom.UO_Zopp
  | SMT_terms.Atom.UO_BVbitOf (s, i) -> SmtAtom.UO_BVbitOf (Camlcoq.N.to_int s, Camlcoq.Nat.to_int i)
  | SMT_terms.Atom.UO_BVnot s -> SmtAtom.UO_BVnot (Camlcoq.N.to_int s)
  | SMT_terms.Atom.UO_BVneg s -> SmtAtom.UO_BVneg (Camlcoq.N.to_int s)
  | SMT_terms.Atom.UO_BVextr (s1, s2, s3) -> SmtAtom.UO_BVextr (Camlcoq.N.to_int s1, Camlcoq.N.to_int s2, Camlcoq.N.to_int s3)
  | SMT_terms.Atom.UO_BVzextn (s1, s2) -> SmtAtom.UO_BVzextn (Camlcoq.N.to_int s1, Camlcoq.N.to_int s2)
  | SMT_terms.Atom.UO_BVsextn (s1, s2) -> SmtAtom.UO_BVsextn (Camlcoq.N.to_int s1, Camlcoq.N.to_int s2)


let b_to_coq = function
  | SmtAtom.BO_Zplus ->  SMT_terms.Atom.BO_Zplus
  | SmtAtom.BO_Zminus -> SMT_terms.Atom.BO_Zminus
  | SmtAtom.BO_Zmult ->  SMT_terms.Atom.BO_Zmult
  | SmtAtom.BO_Zlt ->    SMT_terms.Atom.BO_Zlt
  | SmtAtom.BO_Zle ->    SMT_terms.Atom.BO_Zle
  | SmtAtom.BO_Zge ->    SMT_terms.Atom.BO_Zge
  | SmtAtom.BO_Zgt ->    SMT_terms.Atom.BO_Zgt
  | SmtAtom.BO_eq t ->   SMT_terms.Atom.BO_eq (btype_to_coq t)
  | SmtAtom.BO_BVand s -> SMT_terms.Atom.BO_BVand (mkN s)
  | SmtAtom.BO_BVor s -> SMT_terms.Atom.BO_BVor (mkN s)
  | SmtAtom.BO_BVxor s -> SMT_terms.Atom.BO_BVxor (mkN s)
  | SmtAtom.BO_BVadd s -> SMT_terms.Atom.BO_BVadd (mkN s)
  | SmtAtom.BO_BVmult s -> SMT_terms.Atom.BO_BVmult (mkN s)
  | SmtAtom.BO_BVult s -> SMT_terms.Atom.BO_BVult (mkN s)
  | SmtAtom.BO_BVslt s -> SMT_terms.Atom.BO_BVslt (mkN s)
  | SmtAtom.BO_BVconcat (s1, s2) -> SMT_terms.Atom.BO_BVconcat (mkN s1, mkN s2)
  | SmtAtom.BO_BVshl s -> SMT_terms.Atom.BO_BVshl (mkN s)
  | SmtAtom.BO_BVshr s -> SMT_terms.Atom.BO_BVshr (mkN s)
  | SmtAtom.BO_select (k ,v) -> SMT_terms.Atom.BO_select (btype_to_coq k, btype_to_coq v)
  | SmtAtom.BO_diffarray (k ,v) -> SMT_terms.Atom.BO_diffarray (btype_to_coq k, btype_to_coq v)

let b_to_coq' = function
  | SMT_terms.Atom.BO_Zplus ->  SmtAtom.BO_Zplus
  | SMT_terms.Atom.BO_Zminus -> SmtAtom.BO_Zminus
  | SMT_terms.Atom.BO_Zmult ->  SmtAtom.BO_Zmult
  | SMT_terms.Atom.BO_Zlt ->    SmtAtom.BO_Zlt
  | SMT_terms.Atom.BO_Zle ->    SmtAtom.BO_Zle
  | SMT_terms.Atom.BO_Zge ->    SmtAtom.BO_Zge
  | SMT_terms.Atom.BO_Zgt ->    SmtAtom.BO_Zgt
  | SMT_terms.Atom.BO_eq t ->   SmtAtom.BO_eq (btype_to_coq' t)
  | SMT_terms.Atom.BO_BVand s -> SmtAtom.BO_BVand (Camlcoq.N.to_int s)
  | SMT_terms.Atom.BO_BVor s -> SmtAtom.BO_BVor (Camlcoq.N.to_int s)
  | SMT_terms.Atom.BO_BVxor s -> SmtAtom.BO_BVxor (Camlcoq.N.to_int s)
  | SMT_terms.Atom.BO_BVadd s -> SmtAtom.BO_BVadd (Camlcoq.N.to_int s)
  | SMT_terms.Atom.BO_BVmult s -> SmtAtom.BO_BVmult (Camlcoq.N.to_int s)
  | SMT_terms.Atom.BO_BVult s -> SmtAtom.BO_BVult (Camlcoq.N.to_int s)
  | SMT_terms.Atom.BO_BVslt s -> SmtAtom.BO_BVslt (Camlcoq.N.to_int s)
  | SMT_terms.Atom.BO_BVconcat (s1, s2) -> SmtAtom.BO_BVconcat (Camlcoq.N.to_int s1, Camlcoq.N.to_int s2)
  | SMT_terms.Atom.BO_BVshl s -> SmtAtom.BO_BVshl (Camlcoq.N.to_int s)
  | SMT_terms.Atom.BO_BVshr s -> SmtAtom.BO_BVshr (Camlcoq.N.to_int s)
  | SMT_terms.Atom.BO_select (k ,v) -> SmtAtom.BO_select (btype_to_coq' k, btype_to_coq' v)
  | SMT_terms.Atom.BO_diffarray (k ,v) -> SmtAtom.BO_diffarray (btype_to_coq' k, btype_to_coq' v)


let tInt x = snd (Uint63.to_int2 x)
let gInt x = SmtAtom.Index (tInt x)

let t_to_coq = function
  | SmtAtom.TO_store (k ,v) -> SMT_terms.Atom.TO_store (btype_to_coq k, btype_to_coq v)

let t_to_coq' = function
  | SMT_terms.Atom.TO_store (k ,v) -> SmtAtom.TO_store (btype_to_coq' k, btype_to_coq' v)


let n_to_coq = function
  | SmtAtom.NO_distinct t -> btype_to_coq t

let n_to_coq' = function
  | t -> SmtAtom.NO_distinct (btype_to_coq' t)


let i_to_coq i = mkInt (SmtAtom.indexed_op_index i)

let i_to_coq' i = SmtAtom.dummy_indexed_op (gInt i) (Array.make 0 SmtBtype.Tbool) SmtBtype.TZ


let hatom_to_coq h = mkInt (SmtAtom.Atom.index h)

let a_to_coq a =
  match a with
    | SmtAtom.Acop op -> SMT_terms.Atom.Acop (c_to_coq op)
    | SmtAtom.Auop (op,h) -> SMT_terms.Atom.Auop (u_to_coq op, hatom_to_coq h)
    | SmtAtom.Abop (op,h1,h2) ->
      SMT_terms.Atom.Abop (b_to_coq op, hatom_to_coq h1, hatom_to_coq h2)
    | SmtAtom.Atop (op,h1,h2,h3) ->
      SMT_terms.Atom.Atop (t_to_coq op, hatom_to_coq h1, hatom_to_coq h2, hatom_to_coq h3)
    | SmtAtom.Anop (op,ha) ->
      let cop = n_to_coq op in
      let cargs = Array.fold_right (fun h l -> (hatom_to_coq h :: l)) ha [] in
      SMT_terms.Atom.Anop (cop, cargs)
    | SmtAtom.Aapp (op,args) ->
      let cop = i_to_coq op in
      let cargs = Array.fold_right (fun h l -> (hatom_to_coq h :: l)) args [] in
      SMT_terms.Atom.Aapp (cop, cargs)

let atom_interp_tbl reify =
  let t = SmtAtom.Atom.to_array reify (SMT_terms.Atom.Acop SMT_terms.Atom.CO_xH) a_to_coq in
  mkArray t

let form_to_coq hf = mkInt (SmtAtom.Form.to_lit hf)

let args_to_coq args =
  let cargs = Array.make (Array.length args + 1) (mkInt 0) in
  Array.iteri (fun i hf -> cargs.(i) <- form_to_coq hf) args;
  mkArray cargs

let pf_to_coq pf =
  match pf with
  | SmtForm.Fatom a -> SMT_terms.Form.Fatom (hatom_to_coq a)
  | SmtForm.Fapp(op,args) ->
     (match op with
        | SmtForm.Ftrue -> SMT_terms.Form.Ftrue
        | SmtForm.Ffalse -> SMT_terms.Form.Ffalse
        | SmtForm.Fand -> SMT_terms.Form.Fand (args_to_coq args)
        | SmtForm.For  -> SMT_terms.Form.For (args_to_coq args)
        | SmtForm.Fxor -> if Array.length args = 2 then SMT_terms.Form.Fxor (form_to_coq args.(0), form_to_coq args.(1)) else assert false
        | SmtForm.Fimp -> SMT_terms.Form.Fimp (args_to_coq args)
        | SmtForm.Fiff -> if Array.length args = 2 then SMT_terms.Form.Fiff (form_to_coq args.(0), form_to_coq args.(1)) else assert false
        | SmtForm.Fite -> if Array.length args = 3 then SMT_terms.Form.Fite (form_to_coq args.(0), form_to_coq args.(1), form_to_coq args.(2)) else assert false
        | SmtForm.Fnot2 i -> SMT_terms.Form.Fnot2 (mkInt i, form_to_coq args.(0))
        | SmtForm.Fforall _ -> failwith error_quant
     )
  | SmtForm.FbbT (a, args) -> SMT_terms.Form.FbbT (hatom_to_coq a, dump_list form_to_coq args)


let form_interp_tbl reify =
  let (_,t) = SmtAtom.Form.to_array reify SMT_terms.Form.Ftrue pf_to_coq in
  mkArray t

let transformer a_type a_atom a_form d ra rf =

  let atom: Uint63.t -> SMT_terms.Atom.atom = PArray.get a_atom in
  let func: Uint63.t -> SMT_terms.Form.form = PArray.get a_form in

  let rec a_to_coq' (a: SMT_terms.Atom.atom) : SmtAtom.atom =
    match a with
      | SMT_terms.Atom.Acop op -> SmtAtom.Acop (c_to_coq' op)
      | SMT_terms.Atom.Auop (op,h) -> SmtAtom.Auop (u_to_coq' op, hatom_to_coq' h)
      | SMT_terms.Atom.Abop (op,h1,h2) ->
        SmtAtom.Abop (b_to_coq' op, hatom_to_coq' h1, hatom_to_coq' h2)
      | SMT_terms.Atom.Atop (op,h1,h2,h3) ->
        SmtAtom.Atop (t_to_coq' op, hatom_to_coq' h1, hatom_to_coq' h2, hatom_to_coq' h3)
      | SMT_terms.Atom.Anop (op,ha) -> failwith "Not implemented 1..."
      | _ -> failwith "Not implemented 2..."
  and hatom_to_coq' (h: Uint63.t) : SmtAtom.hatom =
    SmtAtom.Atom.set ra (tInt h) (a_to_coq' (atom h))
  in

  let rec pf_to_coq' (pf: SMT_terms.Form.form): SmtAtom.Form.pform =
    match pf with
    | SMT_terms.Form.Fatom a -> SmtForm.Fatom (hatom_to_coq' a)
    | SMT_terms.Form.Ftrue -> SmtForm.Fapp (SmtForm.Ftrue, [||])
    | SMT_terms.Form.Ffalse -> SmtForm.Fapp (SmtForm.Ffalse, [||])
    | SMT_terms.Form.Fand args -> SmtForm.Fapp (SmtForm.Fand, args_to_coq' args)
    | SMT_terms.Form.For args -> SmtForm.Fapp (SmtForm.For, args_to_coq' args)
    | SMT_terms.Form.Fite (a,b,c) -> SmtForm.Fapp (SmtForm.For,
        [|form_to_coq' a; form_to_coq' b; form_to_coq' c|])
    | _ -> failwith "Not THERE..."

  and form_to_coq' i = SmtAtom.Form.set rf (tInt i) (pf_to_coq' (func (Uint63.div i (mkInt 2))))

  and args_to_coq' args =
    Array.of_list (List.map form_to_coq' (Misc.to_list args))
  in

  Array.to_list (args_to_coq' d)


(* Importing from SMT-LIB v.2 without generating section variables *)
(* From smtlib2/smtlib2_genConstr.ml *)
let count_btype = ref 0
let count_op = ref 0


let declare_sort sym =
  let s = Smtlib2_genConstr.string_of_symbol sym in
  let res = SmtBtype.Tindex (SmtBtype.dummy_indexed_type !count_btype) in
  incr count_btype;
  SmtMaps.add_btype s res;
  res


let declare_fun sym arg cod =
  let s = Smtlib2_genConstr.string_of_symbol sym in
  let tyl = List.map Smtlib2_genConstr.sort_of_sort arg in
  let ty = Smtlib2_genConstr.sort_of_sort cod in
  let op = SmtAtom.dummy_indexed_op (SmtAtom.Index !count_op) (Array.of_list tyl) ty in
  incr count_op;
  SmtMaps.add_fun s op;
  op


let declare_commands ra rf acc = function
  | Smtlib2_ast.CDeclareSort (_,sym,_) -> let _ = declare_sort sym in acc
  | Smtlib2_ast.CDeclareFun (_,sym, (_, arg), cod) -> let _ = declare_fun sym arg cod in acc
  | Smtlib2_ast.CAssert (_, t) -> (Smtlib2_genConstr.make_root ra rf t)::acc
  | _ -> acc


let import_smtlib2 ra rf filename =
  let chan = open_in filename in
  let lexbuf = Lexing.from_channel chan in
  let commands = Smtlib2_parse.main Smtlib2_lex.token lexbuf in
  close_in chan;
  match commands with
    | None -> []
    | Some (Smtlib2_ast.Commands (_,(_,res))) ->
      List.rev (List.fold_left (declare_commands ra rf) [] res)


(* The final checker *)

let this_clear_all () =
  Verit.clear_all ();
  count_btype := 0;
  count_op := 0


let certif_ops =
  ((fun (a, b) -> Trace.Checker_Ext.Res (a, b)),
   (fun (a, b, c) -> Trace.Checker_Ext.Weaken (a, b, c)),
   (fun (a, b, c) -> Trace.Checker_Ext.ImmFlatten (a, b, c)),
   (fun a -> Trace.Checker_Ext.CTrue a),
   (fun a -> Trace.Checker_Ext.CFalse a),
   (fun (a, b) -> Trace.Checker_Ext.BuildDef (a, b)),
   (fun (a, b) -> Trace.Checker_Ext.BuildDef2 (a, b)),
   (fun (a, b, c) -> Trace.Checker_Ext.BuildProj (a, b, c)),
   (fun (a, b, c) -> Trace.Checker_Ext.ImmBuildProj (a, b, c)),
   (fun (a, b) -> Trace.Checker_Ext.ImmBuildDef (a, b)),
   (fun (a, b) -> Trace.Checker_Ext.ImmBuildDef2 (a, b)),
   (fun (a, b, c) -> Trace.Checker_Ext.EqTr (a, b, c)),
   (fun (a, b, c) -> Trace.Checker_Ext.EqCgr (a, b, c)),
   (fun (a, b, c, d) -> Trace.Checker_Ext.EqCgrP (a, b, c, d)),
   (fun (a, b, c) -> Trace.Checker_Ext.LiaMicromega (a, b, c)),
   (fun (a, b) -> Trace.Checker_Ext.LiaDiseq (a, b)),
   (fun  (a, b, c, d) -> Trace.Checker_Ext.SplArith (a, b, c, d)),
   (fun (a, b, c) -> Trace.Checker_Ext.SplDistinctElim (a, b, c)),
   (fun (a, b) -> Trace.Checker_Ext.BBVar (a, b)),
   (fun (a, b) -> Trace.Checker_Ext.BBConst (a, b)),
   (fun (a, b, c, d) -> Trace.Checker_Ext.BBOp (a, b, c, d)),
   (fun (a, b, c) -> Trace.Checker_Ext.BBNot (a, b, c)),
   (fun (a, b, c, d) -> Trace.Checker_Ext.BBEq (a, b, c, d)),
   (fun (a, b) -> Trace.Checker_Ext.BBDiseq (a, b)),
   (fun (a, b, c) -> Trace.Checker_Ext.BBNeg (a, b, c)),
   (fun (a, b, c, d) -> Trace.Checker_Ext.BBAdd (a, b, c, d)),
   (fun (a, b, c, d) -> Trace.Checker_Ext.BBMul (a, b, c, d)),
   (fun (a, b, c, d) -> Trace.Checker_Ext.BBUlt (a, b, c, d)),
   (fun (a, b, c, d) -> Trace.Checker_Ext.BBSlt (a, b, c, d)),
   (fun (a, b, c, d) -> Trace.Checker_Ext.BBConcat (a, b, c, d)),
   (fun (a, b, c) -> Trace.Checker_Ext.BBExtract (a, b, c)),
   (fun (a, b, c) -> Trace.Checker_Ext.BBZextend (a, b, c)),
   (fun (a, b, c) -> Trace.Checker_Ext.BBSextend (a, b, c)),
   (fun (a, b, c, d) -> Trace.Checker_Ext.BBShl (a, b, c, d)),
   (fun (a, b, c, d) -> Trace.Checker_Ext.BBShr (a, b, c, d)),
   (fun (a, b) -> Trace.Checker_Ext.RowEq (a, b)),
   (fun (a, b) -> Trace.Checker_Ext.RowNeq (a, b)),
   (fun (a, b) -> Trace.Checker_Ext.Ext (a, b)),
   (fun () -> failwith error_hole),
   (fun () -> failwith error_quant))

let to_smt fr ar fmt h =

  let atom = PArray.get ar in
  let func = PArray.get fr in

  let to_smt_int i =
    let s1 = if i < 0 then "(- " else "" in
    let s2 = if i < 0 then ")" else "" in
    let j = if i < 0 then -i else i in
    Format.fprintf fmt "%s%i%s" s1 j s2
  in

  let rec compute_int = function
    | SMT_terms.Atom.Acop c ->
      (match c with
       | CO_xH -> 1
       | CO_Z0 -> 0
       | CO_BV _ -> assert false)
    | SMT_terms.Atom.Auop (op,h) ->
      (match op with
       | UO_xO -> 2*(compute_hint h)
       | UO_xI -> 2*(compute_hint h) + 1
       | UO_Zpos -> compute_hint h
       | UO_Zneg -> - (compute_hint h)
       | UO_Zopp | UO_BVbitOf _
       | UO_BVnot _ | UO_BVneg _
       | UO_BVextr _ | UO_BVzextn _ | UO_BVsextn _ -> assert false)
    | _ -> assert false

  and compute_hint h = compute_int (atom h) in

  let rec to_smt _ h =
    if Uint63.rem h (mkInt 2) = mkInt 0 then
      form_to_smt (func (Uint63.div h (mkInt 2)))
    else
      (Format.fprintf fmt "(not ";
       form_to_smt (func (Uint63.div h (mkInt 2)));
       Format.fprintf fmt ")")

  and args_to_smt _ args =
    List.iter (fun h -> Format.fprintf fmt " "; to_smt fmt h)
      (Misc.to_list args)

  and form_to_smt op =
    match op with
    | SMT_terms.Form.Fatom a -> atom_to_smt fmt (atom a)
    | SMT_terms.Form.Ftrue -> Format.fprintf fmt "true"
    | SMT_terms.Form.Ffalse -> Format.fprintf fmt "false"
    | SMT_terms.Form.Fand args -> Format.fprintf fmt "(and%a)" args_to_smt args
    | SMT_terms.Form.For args -> Format.fprintf fmt "(or%a)" args_to_smt args
    | SMT_terms.Form.Fxor (a,b) -> Format.fprintf fmt "(xor %a %a)" to_smt a to_smt b
    | SMT_terms.Form.Fimp args -> Format.fprintf fmt "(=>%a)" args_to_smt args
    | SMT_terms.Form.Fiff (a,b) -> Format.fprintf fmt "(= %a %a)" to_smt a to_smt b
    | SMT_terms.Form.Fite (a,b,c) -> Format.fprintf fmt "(ite %a %a %a)" to_smt a to_smt b to_smt c
    | SMT_terms.Form.Fnot2 _ -> ()
    | _ -> ()

  and atom_to_smt _ = function
    | SMT_terms.Atom.Aapp (f,args) -> Format.fprintf fmt "op_%d" (tInt f)
    | SMT_terms.Atom.Acop _ as a -> to_smt_int (compute_int a)
    | SMT_terms.Atom.Auop (op,h) -> to_smt_uop op h
    | SMT_terms.Atom.Abop (op,h1,h2) -> to_smt_bop op h1 h2
    | SMT_terms.Atom.Atop (op,h1,h2,h3) -> to_smt_top op h1 h2 h3
    | SMT_terms.Atom.Anop (op,a) -> to_smt_nop op a

  and to_smt_uop op h =
    match op with
    | SMT_terms.Atom.UO_Zpos ->
      Format.fprintf fmt "%a" atom_to_smt (atom h)
    | SMT_terms.Atom.UO_Zneg ->
      Format.fprintf fmt "(- %a)" atom_to_smt (atom h)
    | SMT_terms.Atom.UO_Zopp ->
      Format.fprintf fmt "(- %a)" atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVbitOf (_, i) ->
      Format.fprintf fmt "(bitof %d %a)" (Camlcoq.Nat.to_int i) atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVnot _ ->
      Format.fprintf fmt "(bvnot %a)" atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVneg _ ->
      Format.fprintf fmt "(bvneg %a)" atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVextr (i, n, _) ->
      Format.fprintf fmt "((_ extract %d %d) %a)" (Camlcoq.N.to_int i+Camlcoq.N.to_int n-1) (Camlcoq.N.to_int i) atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVzextn (_, n) ->
      Format.fprintf fmt "((_ zero_extend %d) %a)" (Camlcoq.N.to_int n) atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVsextn (_, n) ->
      Format.fprintf fmt "((_ sign_extend %d) %a)" (Camlcoq.N.to_int n) atom_to_smt (atom h)
    | _ -> to_smt_int (compute_int (Auop (op, h)))

  and to_smt_bop op h1 h2 =
    let s = match op with
      | SMT_terms.Atom.BO_Zplus -> "+"
      | SMT_terms.Atom.BO_Zminus -> "-"
      | SMT_terms.Atom.BO_Zmult -> "*"
      | SMT_terms.Atom.BO_Zlt -> "<"
      | SMT_terms.Atom.BO_Zle -> "<="
      | SMT_terms.Atom.BO_Zge -> ">="
      | SMT_terms.Atom.BO_Zgt -> ">"
      | SMT_terms.Atom.BO_eq _ -> "="
      | SMT_terms.Atom.BO_BVand _ -> "bvand"
      | SMT_terms.Atom.BO_BVor _ -> "bvor"
      | SMT_terms.Atom.BO_BVxor _ -> "bvxor"
      | SMT_terms.Atom.BO_BVadd _ -> "bvadd"
      | SMT_terms.Atom.BO_BVmult _ -> "bvmul"
      | SMT_terms.Atom.BO_BVult _ -> "bvult"
      | SMT_terms.Atom.BO_BVslt _ -> "bvslt"
      | SMT_terms.Atom.BO_BVconcat _ -> "concat"
      | SMT_terms.Atom.BO_BVshl _ -> "bvshl"
      | SMT_terms.Atom.BO_BVshr _ -> "bvlshr"
      | SMT_terms.Atom.BO_select _ -> "select"
      | SMT_terms.Atom.BO_diffarray _ -> "diff" (* should not be used in goals *)
    in
    Format.fprintf fmt "(%s %a %a)" s atom_to_smt (atom h1) atom_to_smt (atom h2)

  and to_smt_top op h1 h2 h3 =
    let s = match op with
      | TO_store _ -> "store"
    in
    Format.fprintf fmt "(%s %a %a %a)" s atom_to_smt (atom h1) atom_to_smt (atom h2) atom_to_smt (atom h3)

  and to_smt_nop op a =
    let s = "distinct" in
    Format.fprintf fmt "(%s" s;
    List.iter (fun h -> Format.fprintf fmt " "; atom_to_smt fmt (atom h)) a;
    Format.fprintf fmt ")"

  in
  to_smt fmt h

let ofInt x = snd (Uint63.to_int2 x)

let to_smt2 fr ar fmt h =

  let atom x = Format.fprintf fmt "ATOM: %d\n" (ofInt x); PArray.get ar x in
  let func x = Format.fprintf fmt "FUNC: %d\n" (ofInt x); PArray.get fr x in

  let to_smt_int i =
    let s1 = if i < 0 then "(- " else "" in
    let s2 = if i < 0 then ")" else "" in
    let j = if i < 0 then -i else i in
    Format.fprintf fmt "%s%i%s" s1 j s2
  in

  let rec compute_int = function
    | SMT_terms.Atom.Acop c ->
      (match c with
       | CO_xH -> Format.fprintf fmt "CO_xH\n"; 1
       | CO_Z0 -> Format.fprintf fmt "CO_Z0\n"; 0
       | CO_BV _ -> assert false)
    | SMT_terms.Atom.Auop (op,h) ->
      (match op with
       | UO_xO -> Format.fprintf fmt "UO_xO\n"; 2*(compute_hint h)
       | UO_xI -> Format.fprintf fmt "UO_xI\n"; 2*(compute_hint h) + 1
       | UO_Zpos -> compute_hint h
       | UO_Zneg -> - (compute_hint h)
       | UO_Zopp | UO_BVbitOf _
       | UO_BVnot _ | UO_BVneg _
       | UO_BVextr _ | UO_BVzextn _ | UO_BVsextn _ -> assert false)
    | _ -> assert false

  and compute_hint h = Format.fprintf fmt "%d: " (ofInt h); compute_int (atom h) in

  let rec to_smt2 _ h =
    Format.fprintf fmt "GET: %d\n" (ofInt h);
    if Uint63.rem h (mkInt 2) = mkInt 0 then
      form_to_smt (func (Uint63.div h (mkInt 2)))
    else
      (Format.fprintf fmt "(not ";
       form_to_smt (func (Uint63.div h (mkInt 2)));
       Format.fprintf fmt ")")

  and args_to_smt _ args =
    List.iter (fun h -> Format.fprintf fmt " "; to_smt2 fmt h)
      (Misc.to_list args)

  and form_to_smt op =
    match op with
    | SMT_terms.Form.Fatom a -> atom_to_smt fmt (atom a)
    | SMT_terms.Form.Ftrue -> Format.fprintf fmt "true"
    | SMT_terms.Form.Ffalse -> Format.fprintf fmt "false"
    | SMT_terms.Form.Fand args -> Format.fprintf fmt "(and%a)" args_to_smt args
    | SMT_terms.Form.For args -> Format.fprintf fmt "(or%a)" args_to_smt args
    | SMT_terms.Form.Fxor (a,b) -> Format.fprintf fmt "(xor %a %a)" to_smt2 a to_smt2 b
    | SMT_terms.Form.Fimp args -> Format.fprintf fmt "(=>%a)" args_to_smt args
    | SMT_terms.Form.Fiff (a,b) -> Format.fprintf fmt "(= %a %a)" to_smt2 a to_smt2 b
    | SMT_terms.Form.Fite (a,b,c) -> Format.fprintf fmt "(ite %a %a %a)" to_smt2 a to_smt2 b to_smt2 c
    | SMT_terms.Form.Fnot2 _ -> ()
    | _ -> ()

  and atom_to_smt _ x =
    match x with
    | SMT_terms.Atom.Aapp (f,args) -> ()
    | SMT_terms.Atom.Acop _ as a -> Format.fprintf fmt "ACOP\n"; to_smt_int (compute_int a)
    | SMT_terms.Atom.Auop (op,h) -> Format.fprintf fmt "UOP\n"; to_smt_uop op h
    | SMT_terms.Atom.Abop (op,h1,h2) -> Format.fprintf fmt "BOP\n"; to_smt_bop op h1 h2
    | SMT_terms.Atom.Atop (op,h1,h2,h3) -> Format.fprintf fmt "ATOP\n"; to_smt_top op h1 h2 h3
    | SMT_terms.Atom.Anop (op,a) -> Format.fprintf fmt "ANOP\n"; to_smt_nop op a

  and to_smt_uop op h =
    match op with
    | SMT_terms.Atom.UO_Zpos ->
      Format.fprintf fmt ":: ZPos :: %a" atom_to_smt (atom h)
    | SMT_terms.Atom.UO_Zneg ->
      Format.fprintf fmt "(- %a)" atom_to_smt (atom h)
    | SMT_terms.Atom.UO_Zopp ->
      Format.fprintf fmt "(- %a)" atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVbitOf (_, i) ->
      Format.fprintf fmt "(bitof %d %a)" (Camlcoq.Nat.to_int i) atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVnot _ ->
      Format.fprintf fmt "(bvnot %a)" atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVneg _ ->
      Format.fprintf fmt "(bvneg %a)" atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVextr (i, n, _) ->
      Format.fprintf fmt "((_ extract %d %d) %a)" (Camlcoq.N.to_int i+Camlcoq.N.to_int n-1) (Camlcoq.N.to_int i) atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVzextn (_, n) ->
      Format.fprintf fmt "((_ zero_extend %d) %a)" (Camlcoq.N.to_int n) atom_to_smt (atom h)
    | SMT_terms.Atom.UO_BVsextn (_, n) ->
      Format.fprintf fmt "((_ sign_extend %d) %a)" (Camlcoq.N.to_int n) atom_to_smt (atom h)
    | _ -> to_smt_int (compute_int (Auop (op, h)))

  and to_smt_bop op h1 h2 =
    let s = match op with
      | SMT_terms.Atom.BO_Zplus -> "+"
      | SMT_terms.Atom.BO_Zminus -> "-"
      | SMT_terms.Atom.BO_Zmult -> "*"
      | SMT_terms.Atom.BO_Zlt -> "<"
      | SMT_terms.Atom.BO_Zle -> "<="
      | SMT_terms.Atom.BO_Zge -> ">="
      | SMT_terms.Atom.BO_Zgt -> ">"
      | SMT_terms.Atom.BO_eq _ -> "="
      | SMT_terms.Atom.BO_BVand _ -> "bvand"
      | SMT_terms.Atom.BO_BVor _ -> "bvor"
      | SMT_terms.Atom.BO_BVxor _ -> "bvxor"
      | SMT_terms.Atom.BO_BVadd _ -> "bvadd"
      | SMT_terms.Atom.BO_BVmult _ -> "bvmul"
      | SMT_terms.Atom.BO_BVult _ -> "bvult"
      | SMT_terms.Atom.BO_BVslt _ -> "bvslt"
      | SMT_terms.Atom.BO_BVconcat _ -> "concat"
      | SMT_terms.Atom.BO_BVshl _ -> "bvshl"
      | SMT_terms.Atom.BO_BVshr _ -> "bvlshr"
      | SMT_terms.Atom.BO_select _ -> "select"
      | SMT_terms.Atom.BO_diffarray _ -> "diff" (* should not be used in goals *)
    in
    Format.fprintf fmt "(%s %a %a)" s atom_to_smt (atom h1) atom_to_smt (atom h2)

  and to_smt_top op h1 h2 h3 =
    let s = match op with
      | TO_store _ -> "store"
    in
    Format.fprintf fmt "(%s %a %a %a)" s atom_to_smt (atom h1) atom_to_smt (atom h2) atom_to_smt (atom h3)

  and to_smt_nop op a =
    let s = "distinct" in
    Format.fprintf fmt "(%s" s;
    List.iter (fun h -> Format.fprintf fmt " "; atom_to_smt fmt (atom h)) a;
    Format.fprintf fmt ")"

  in
  to_smt2 fmt h

let print_int63 fmt x = Format.fprintf fmt "%d" (snd (Uint63.to_int2 x))

let print_cop fmt = function
  | SMT_terms.Atom.CO_xH -> Format.fprintf fmt "CO_xH"
  | SMT_terms.Atom.CO_Z0 -> Format.fprintf fmt "CO_Z0"
  | SMT_terms.Atom.CO_BV _ -> Format.fprintf fmt "CO_BV"

let print_uop fmt = function
  | SMT_terms.Atom.UO_xO -> Format.fprintf fmt "UO_xO"
  | SMT_terms.Atom.UO_xI -> Format.fprintf fmt "UO_xI"
  | SMT_terms.Atom.UO_Zpos -> Format.fprintf fmt "UO_Zpos"
  | SMT_terms.Atom.UO_Zneg -> Format.fprintf fmt "UO_Zneg"
  | SMT_terms.Atom.UO_Zopp -> Format.fprintf fmt "UO_Zopp"
  | SMT_terms.Atom.UO_BVbitOf _ -> Format.fprintf fmt "UO_BVbitOf"
  | SMT_terms.Atom.UO_BVnot _ -> Format.fprintf fmt "UO_BVnot"
  | SMT_terms.Atom.UO_BVneg _ -> Format.fprintf fmt "UO_BVneg"
  | SMT_terms.Atom.UO_BVextr (i, n0, n1) -> Format.fprintf fmt "UO_BVextr"
  | SMT_terms.Atom.UO_BVzextn (n, i) -> Format.fprintf fmt "UO_BVzextn"
  | SMT_terms.Atom.UO_BVsextn (n, i) -> Format.fprintf fmt "UO_BVsextn"

let print_bop fmt = function
  | SMT_terms.Atom.BO_Zplus -> Format.fprintf fmt "BO_Zplus"
  | SMT_terms.Atom.BO_Zminus -> Format.fprintf fmt "BO_Zminus"
  | SMT_terms.Atom.BO_Zmult -> Format.fprintf fmt "BO_Zmult"
  | SMT_terms.Atom.BO_Zlt -> Format.fprintf fmt "BO_Zlt"
  | SMT_terms.Atom.BO_Zle -> Format.fprintf fmt "BO_Zle"
  | SMT_terms.Atom.BO_Zge -> Format.fprintf fmt "BO_Zge"
  | SMT_terms.Atom.BO_Zgt -> Format.fprintf fmt "BO_Zgt"
  | SMT_terms.Atom.BO_eq _ -> Format.fprintf fmt "BO_eq"
  | SMT_terms.Atom.BO_BVand _ -> Format.fprintf fmt "BO_BVand"
  | SMT_terms.Atom.BO_BVor _ -> Format.fprintf fmt "BO_BVor"
  | SMT_terms.Atom.BO_BVxor _ -> Format.fprintf fmt "BO_BVxor"
  | SMT_terms.Atom.BO_BVadd _ -> Format.fprintf fmt "BO_BVadd"
  | SMT_terms.Atom.BO_BVmult _ -> Format.fprintf fmt "BO_BVmult"
  | SMT_terms.Atom.BO_BVult _ -> Format.fprintf fmt "BO_BVult"
  | SMT_terms.Atom.BO_BVslt _ -> Format.fprintf fmt "BO_BVslt"
  | SMT_terms.Atom.BO_BVconcat _ -> Format.fprintf fmt "BO_BVconcat"
  | SMT_terms.Atom.BO_BVshl _ -> Format.fprintf fmt "BO_BVshl"
  | SMT_terms.Atom.BO_BVshr _ -> Format.fprintf fmt "BO_BVshr"
  | SMT_terms.Atom.BO_select _ -> Format.fprintf fmt "BO_select"
  | SMT_terms.Atom.BO_diffarray _ -> Format.fprintf fmt "BO_diffarray"

let print_top fmt = function
  | SMT_terms.Atom.TO_store _ -> Format.fprintf fmt "TO_store"

let print_list el_printer fmt l =
  let first = ref true in
  Format.fprintf fmt "[";
  List.iter (fun x -> if !first
                      then (Format.fprintf fmt "%a" el_printer x; first := false)
                      else Format.fprintf fmt "; %a" el_printer x) l;
  Format.fprintf fmt "]"

let print_list_indx el_printer fmt l =
  let first = ref true in
  let i = ref 0 in
  Format.fprintf fmt "[";
  List.iter (fun x -> (if !first
                       then (Format.fprintf fmt "%d: %a" !i el_printer x; first := false)
                       else Format.fprintf fmt "; %d: %a" !i el_printer x); i := !i + 1) l;
  Format.fprintf fmt "]"

let print_list_nl el_printer fmt l =
  let first = ref true in
  Format.fprintf fmt "[";
  List.iter (fun x -> if !first
                      then (Format.fprintf fmt "%a" el_printer x; first := false)
                      else Format.fprintf fmt ";\n %a" el_printer x) l;
  Format.fprintf fmt "]"

let print_ilist fmt x = print_list print_int63 fmt (Misc.to_list x)

let print_atom fmt = function
  | SMT_terms.Atom.Aapp (f,args) -> Format.fprintf fmt "Aapp %a %a" print_int63 f (print_list print_int63) args
  | SMT_terms.Atom.Acop a -> Format.fprintf fmt "Acop %a" print_cop a
  | SMT_terms.Atom.Auop (op,h) -> Format.fprintf fmt "Auop %a %a" print_uop op print_int63 h
  | SMT_terms.Atom.Abop (op,h1,h2) -> Format.fprintf fmt "Abop %a %a %a" print_bop op print_int63 h1 print_int63 h2
  | SMT_terms.Atom.Atop (op,h1,h2,h3) -> Format.fprintf fmt "Atop %a %a %a %a" print_top op print_int63 h1 print_int63 h2 print_int63 h3
  | SMT_terms.Atom.Anop (op,a) -> Format.fprintf fmt "Anop"

let print_form fmt = function
  | SMT_terms.Form.Fatom a -> Format.fprintf fmt "Fatom %a" print_int63 a
  | SMT_terms.Form.Ftrue -> Format.fprintf fmt "Ftrue"
  | SMT_terms.Form.Ffalse -> Format.fprintf fmt "Ffalse"
  | SMT_terms.Form.Fnot2 _ -> Format.fprintf fmt "Fnot2"
  | SMT_terms.Form.Fand args -> Format.fprintf fmt "Fand %a" print_ilist args
  | SMT_terms.Form.For args -> Format.fprintf fmt "For %a" print_ilist args
  | SMT_terms.Form.Fimp args -> Format.fprintf fmt "Fimp %a" print_ilist args
  | SMT_terms.Form.Fxor (l1,l2) -> Format.fprintf fmt "Fxor %a %a" print_int63 l1 print_int63 l2
  | SMT_terms.Form.Fiff (l1,l2) -> Format.fprintf fmt "Fiff %a %a" print_int63 l1 print_int63 l2
  | SMT_terms.Form.Fite (l1,l2,l3) -> Format.fprintf fmt "Fite %a %a %a" print_int63 l1 print_int63 l2 print_int63 l3
  | SMT_terms.Form.FbbT _ -> Format.fprintf fmt "FbbT"

let print_step fmt = function
  | Trace.Checker_Ext.Res (p,cl) -> Format.fprintf fmt "%a:Res %a" print_int63 p print_ilist cl
  | Trace.Checker_Ext.Weaken _ -> Format.fprintf fmt "Weaken"
  | Trace.Checker_Ext.ImmFlatten _ -> Format.fprintf fmt "ImmFlatten"
  | Trace.Checker_Ext.CTrue _ -> Format.fprintf fmt "CTrue"
  | Trace.Checker_Ext.CFalse _ -> Format.fprintf fmt "CFalse"
  | Trace.Checker_Ext.BuildDef _ -> Format.fprintf fmt "BuildDef"
  | Trace.Checker_Ext.BuildDef2 _ -> Format.fprintf fmt "BuildDef2"
  | Trace.Checker_Ext.BuildProj _ -> Format.fprintf fmt "BuildProj"
  | Trace.Checker_Ext.ImmBuildDef _ -> Format.fprintf fmt "ImmBuildDef"
  | Trace.Checker_Ext.ImmBuildDef2 _ -> Format.fprintf fmt "ImmBuildDef2"
  | Trace.Checker_Ext.ImmBuildProj _ -> Format.fprintf fmt "ImmBuildProj"
  | Trace.Checker_Ext.EqTr _ -> Format.fprintf fmt "EqTr"
  | Trace.Checker_Ext.EqCgr _ -> Format.fprintf fmt "EqCgr"
  | Trace.Checker_Ext.EqCgrP _ -> Format.fprintf fmt "EqCgrP"
  | Trace.Checker_Ext.LiaMicromega (p,cl,c) -> Format.fprintf fmt "%a:LiaMicromega %a" print_int63 p (print_list print_int63) cl
  | Trace.Checker_Ext.LiaDiseq _ -> Format.fprintf fmt "LiaDiseq"
  | Trace.Checker_Ext.SplArith _ -> Format.fprintf fmt "SplArith"
  | Trace.Checker_Ext.SplDistinctElim _ -> Format.fprintf fmt "SplDistinctElim"
  | Trace.Checker_Ext.BBVar _ -> Format.fprintf fmt "BBVar"
  | Trace.Checker_Ext.BBConst _ -> Format.fprintf fmt "BBConst"
  | Trace.Checker_Ext.BBOp _ -> Format.fprintf fmt "BBOp"
  | Trace.Checker_Ext.BBNot _ -> Format.fprintf fmt "BBNot"
  | Trace.Checker_Ext.BBNeg _ -> Format.fprintf fmt "BBNeg"
  | Trace.Checker_Ext.BBAdd _ -> Format.fprintf fmt "BBAdd"
  | Trace.Checker_Ext.BBConcat _ -> Format.fprintf fmt "BBConcat"
  | Trace.Checker_Ext.BBMul _ -> Format.fprintf fmt "BBMul"
  | Trace.Checker_Ext.BBUlt _ -> Format.fprintf fmt "BBUlt"
  | Trace.Checker_Ext.BBSlt _ -> Format.fprintf fmt "BBSlt"
  | Trace.Checker_Ext.BBEq _ -> Format.fprintf fmt "BBEq"
  | Trace.Checker_Ext.BBDiseq _ -> Format.fprintf fmt "BBDiseq"
  | Trace.Checker_Ext.BBExtract _ -> Format.fprintf fmt "BBExtract"
  | Trace.Checker_Ext.BBZextend _ -> Format.fprintf fmt "BBZextend"
  | Trace.Checker_Ext.BBSextend _ -> Format.fprintf fmt "BBSextend"
  | Trace.Checker_Ext.BBShl _ -> Format.fprintf fmt "BBShl"
  | Trace.Checker_Ext.BBShr _ -> Format.fprintf fmt "BBShr"
  | Trace.Checker_Ext.RowEq _ -> Format.fprintf fmt "RowEq"
  | Trace.Checker_Ext.RowNeq _ -> Format.fprintf fmt "RowNeq"
  | Trace.Checker_Ext.Ext _ -> Format.fprintf fmt "Ext"

let print_trace fmt (st_l, st) =
  print_step fmt st;
  Format.fprintf fmt ": ";
  print_list print_step fmt st_l;
  Format.fprintf fmt "@."

let print_alist fmt x = print_list_indx print_atom fmt (Misc.to_list x)
let print_flist fmt x = print_list_indx print_form fmt (Misc.to_list x)

let print_both fmt a b =
  Format.fprintf fmt "%a\n%a@." print_flist a print_alist b

let print_all fmt a b c =
  Format.fprintf fmt "%a\n%a\n%a@." print_ilist a print_flist b print_alist c

let export out_channel a_atom a_form d =
  let fmt = Format.formatter_of_out_channel out_channel in
  Format.fprintf fmt "(set-logic UFLIA)@.";

  List.iter (function
      | SMT_terms.Atom.Aapp (f,_) -> Format.fprintf fmt "(declare-fun op_%d () Int)@." (tInt f);
      | _ -> ()
    ) (Misc.to_list a_atom);

  List.iter (fun d' ->
    Format.fprintf fmt "(assert %a)\n" (to_smt a_form a_atom) d')
    (Misc.to_list d);

  Format.fprintf fmt "(check-sat)\n(exit)@."

let export2 out_channel a_type a_atom a_form d =
  let fmt = Format.formatter_of_out_channel out_channel in
  Format.fprintf fmt "(set-logic UFLIA)@.";

  let _ = List.fold_left (fun i _ ->
      let s = "op_"^(string_of_int i) in
      Format.fprintf fmt "(declare-fun %s () Int)@." s; i+1
    ) 0 (Misc.to_list a_type) in

  List.iter (fun d' ->
    Format.fprintf fmt "(assert %a)\n" (to_smt2 a_form a_atom) d')
    (Misc.to_list d);

  Format.fprintf fmt "(check-sat)\n(exit)@."

  (* List.iter (fun a -> *)

  (* ) (Misc.to_list) *)

let cohpred_counter = ref 0

let export3 out_channel lsmt =
  let fmt = Format.formatter_of_out_channel out_channel in
  Format.fprintf fmt "(set-logic UFLIA)@.";

  List.iter (fun u -> Format.fprintf fmt "(assert ";
                      SmtAtom.Form.to_smt fmt u;
                      Format.fprintf fmt ")\n") lsmt;

  Format.fprintf fmt "(check-sat)\n(exit)@."

let rec string_rep n s =
  if n = 0 then "" else s ^ string_rep (n - 1) s

(** This function takes in the atoms, formulas and final statement and should return the used_roots
   as well as the certificate.  To do that it should: 1. transform atom and formulas into SMTLIB,
   2. pass the SMTLIB to VeriT, 3. parse certificate and generate used_roots and certificate.  *)
let smt_certificate
    (atom: SMT_terms.Atom.atom PArray.array)
    (form: SMT_terms.Form.form PArray.array)
    (d: Uint63.t PArray.array) =
  this_clear_all ();
  let debug = false in

  if debug then print_all Format.std_formatter d form atom;

  let ra = VeritSyntax.ra in
  let rf = VeritSyntax.rf in

  (* let roots = transformer atype atom form d ra rf in *)

  (* let chan = open_out "hey.smt2" in *)
  (* export3 stdout roots; *)
  (* close_out chan; *)

  let ra_quant = VeritSyntax.ra_quant in
  let rf_quant = VeritSyntax.rf_quant in
  let fsmt = "file.smt2" in let fproof = "file.vtlog" in let wproof = "file.log" in
  let chan = open_out fsmt in
  export chan atom form d;
  close_out chan;

  (* let chan = open_out "test2.smt2" in *)
  (* export2 chan atype atom form d; *)
  (* close_out chan; *)

  cohpred_counter := !cohpred_counter + 1;
  Printf.fprintf stderr "\rContacting license server: %s" (string_rep !cohpred_counter "."); flush stderr;
  let command = "veriT --proof-prune --proof-merge --proof-with-sharing --cnf-definitional --disable-ackermann --input=smtlib2 --proof=" ^ fproof ^ " " ^ fsmt ^ " 2> " ^ wproof ^ " >/dev/null" in
  let command = "timeout 5m "^command in
  if debug then Format.eprintf "%s@." command;
  (* let t0 = Sys.time () in *)
  let exit_code = Sys.command command in
  (* let t1 = Sys.time () in *)
  (* Format.eprintf "Verit = %.5f@." (t1-.t0); *)
  let roots = import_smtlib2 ra rf fsmt in
  let (max_id, confl) = Verit.import_trace ra_quant rf_quant fproof None [] in

  let (tres,last_root) = to_coq (fun i -> mkInt (SmtAtom.Form.to_lit i)) certif_ops confl in
  if debug then print_trace Format.std_formatter tres;
  let certif =
    Trace.Checker_Ext.Certif (mkInt (max_id + 1), tres, mkInt (SmtTrace.get_pos confl))
  in
  let used_roots = SmtCommands.compute_roots roots last_root in
  if debug then (Printf.printf "used_roots: "; List.iter (fun x -> Printf.printf "%d " x) used_roots; print_endline ""; flush stdout);
  let used_rootsCstr =
    let l = List.length used_roots in
    let res = Array.make (l + 1) (mkInt 0) in
    let i = ref (l-1) in
    List.iter (fun j -> res.(!i) <- mkInt j; decr i) used_roots;
    Some (mkArray res)
  in
  let rootsCstr =
    let res = Array.make (List.length roots + 1) (mkInt 0) in
    let i = ref 0 in
    List.iter (fun j -> res.(!i) <- mkInt (SmtAtom.Form.to_lit j); incr i) roots;
    mkArray res
  in

  let t_atom = atom_interp_tbl ra in
  let t_form = form_interp_tbl rf in

  if debug then (print_ilist Format.std_formatter rootsCstr;
                 Format.fprintf Format.std_formatter "\n";
                 print_both Format.std_formatter t_form t_atom);

  ((((t_form, t_atom), rootsCstr), used_rootsCstr), certif)

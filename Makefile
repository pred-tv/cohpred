VS := $(wildcard theory/*.v)

all: Makefile.coq smtcoq/src/Trace.vo smtcoq/src/SMT_terms.vo
	$(MAKE) -f Makefile.coq theory/Smtpredicate.vo

smtcoq/src/CoqMakefile:
	$(MAKE) -C smtcoq/src CoqMakefile

smtcoq/src/%.vo: smtcoq/src/CoqMakefile
	$(MAKE) -C smtcoq/src -f CoqMakefile $(notdir $@)

smtcoq/src/smtcoq_plugin.cmxa:
	$(MAKE) -C smtcoq

smtcoq/src/extraction/smtcoq_extr.cmx: smtcoq/src/smtcoq_plugin.cmxa
	$(MAKE) -C smtcoq/src/extraction

Makefile.coq: force
	coq_makefile -f _CoqProject $(VS) -o Makefile.coq

force:

.PHONY: all install proof clean extraction test force
